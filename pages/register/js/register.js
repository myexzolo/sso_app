// JS Login //

$(function () {
  showForm('LOGIN');
  $("#username").keypress(function(event){
      var ew = event.which;
      if(ew == 32)
          return true;
      if(48 <= ew && ew <= 57)
          return true;
      if(65 <= ew && ew <= 90)
          return true;
      if(97 <= ew && ew <= 122)
          return true;
      return false;
  });
});


function showForm(action){
  $.post("ajax/formAED.php",{action:action})
    .done(function( data ) {
      $('#action').remove();
      $('#show-form').html(data);
  });
}

function checkAccept()
{
  var checked = $("#accept_consent").prop('checked');
  if(checked)
  {
    $("#btnSubmit").prop("disabled", false);
  }else
  {
    $("#btnSubmit").prop("disabled", true);
  }
}

function gologin()
{
  gotoPage("../login");
}

$('#formAED').on('submit', function(event) {
  event.preventDefault();
  // $('.icons').remove('span');
  if ($('#formAED').smkValidate()) {
    if( $.smkEqualPass('#pass1', '#pass2') ){
      $.ajax({
          url: 'ajax/AED.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        // console.log(data);
        // $.smkProgressBar({
        //   element:'body',
        //   status:'start',
        //   bgColor: '#000',
        //   barColor: '#fff',
        //   content: 'Loading...'
        // });
        // setTimeout(function(){
          // $.smkProgressBar({status:'end'});
          //console.log(data);
          $('#formAED').smkClear();
          $.smkAlert({text: data.message,type: data.status});
          if(data.status == 'success'){
            window.location = '../../pages/home/';
          }
        // }, 1000);
      }).fail(function (jqXHR, textStatus) {
          console.log(jqXHR);
      });
    }
  }
});
