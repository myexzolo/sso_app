<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$sql   = "SELECT * FROM t_consent WHERE consent_id = '1'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'][0];

$bg         = "background-color:#fff;";

?>
<div class="modal-body">
  <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>ชื่อ</label>
          <input type="text" value="" name="mem_name" class="form-control" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>นามสกุล</label>
          <input type="text" value="" name="mem_last" class="form-control"  required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>เลขที่บัตรประชาชน</label>
          <input type="text" value="" name="mem_idcard" OnKeyPress="return chkNumber(this)" maxlength="13" class="form-control"  required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>วันเดือนปีเกิด</label>
          <input data-smk-msg="&nbsp;" class="form-control datepicker" value="" required name="mem_dob" type="text" data-provide="datepicker" data-date-language="th-th"  style="<?=$bg ?>">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>เบอร์โทรศัพท์มือถือ</label>
          <input value="" OnKeyPress="return chkNumber(this)" name="tel_no" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
        </div>
      </div>
      <div class="col-md-8">
        <div class="form-group">
          <label>อีเมล์</label>
          <input value="" name="email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>รหัสผุ้ใช้งาน (ภาษาอังกฤษและตัวเลขเท่านั้น)</label>
          <input value="" name="username" id="username" type="text" class="form-control" placeholder="" required data-smk-msg="&nbsp;">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>รหัสผ่าน (6 ตัวอักษรขึ้นไป)</label>
          <input value="" name="mem_pass" id="pass1" type="password" data-smk-strongPass="weak" class="form-control" placeholder="" required data-smk-msg="&nbsp;">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>ยืนยันรหัสผ่าน</label>
          <input value="" id="pass2" type="password" data-smk-strongPass="weak" class="form-control" placeholder="" required data-smk-msg="&nbsp;">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group" style="margin-bottom: 0px;">
          <label>นโยบายความเป็นส่วนตัว</label>
          <div style="height:200px;width:100%;border:1px solid #ccc;overflow:auto;text-align:justify;padding:10px;font-size: 18px">
            <?= $row['consent_desc'] ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="checkbox">
          <div class="form-group" style="margin-bottom: 0px;">
          <label> <input type="checkbox" value="Y" name="accept_consent"  id="accept_consent"  onclick="checkAccept()"  required>ข้าพเจ้ายอมรับข้อกำหนดและเงื่อนไขการให้บริการนี้</label>
          <div/>
        </div>
        <?php
          $sql2   = "SELECT * FROM t_consent_data WHERE consent_id = '1'";

          $query      = DbQuery($sql2,null);
          $json       = json_decode($query, true);
          $dataCount  = $json['dataCount'];
          $row2       = $json['data'];
          if($dataCount > 0)
          {
            foreach ($row2 as $key => $value)
            {
        ?>
        <div class="radio">
          <label> <input value="<?= $value['consent_data_id']."|".$value['hide_list'] ?>"  name="hide_list" type="radio"><?= $value['consent_data_name'] ?></label>
        </div>
        <?php
          }
          }
        ?>
      </div>
    </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" onclick="gologin()">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;" id="btnSubmit" disabled>บันทึก</button>
</div>
