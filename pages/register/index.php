<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบสนับสุนนการให้บริการ (Queue) - เข้าสู่ระบบ</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/register.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">

<div class="row">
  <div class="reg-box">
    <!-- /.login-logo -->
    <div class="col-md-6 col-md-offset-3">
      <div class="reg-box-body">
        <div align="center">
          <div class="login-logo">
            <img src="../../image/logo.png" style="height:120px;">
            <h2 style="color:#005c9e">ระบบสนับสุนนการให้บริการ (Queue)</h2>
          </div>
        </div>
        <p class="login-box-msg">ลงทะเบียนผู้ใช้งาน</p>
        <div class="modal-content">
          <form id="formAED" novalidate enctype="multipart/form-data">
          <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
            <div id="show-form"></div>
          </form>
        </div>
      </div>
      <!-- /.login-box-body -->
    </div>
  </div>
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/register.js"></script>
</body>
</html>
