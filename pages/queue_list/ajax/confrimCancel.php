<?
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">

<?php
    //echo "xxxxxxxxxxxxxxxx";
    //print_r($_POST['jsonData']);

    $jsonData = urldecode($_POST['jsonData']);

    $arrData  = json_decode($jsonData, true);
?>
  <table style="margin:10px;">
    <tr>
      <td style="width: 139px;padding:5px">สำนักงานประกันสังคม</td>
      <td style="width: 14px;text-align:center;padding:5px">:</td>
      <td style="padding:5px"><?= $arrData['agency_name'] ?></td>
    </tr>
    <tr>
      <td style="padding:5px">รหัสการจอง</td>
      <td style="text-align:center;padding:5px">:</td>
      <td style="padding:5px"><?= $arrData['ref_queue'] ?></td>
    </tr>
   <tr>
     <td style="padding:5px">งานบริการ</td>
     <td style="text-align:center;padding:5px">:</td>
     <td style="padding:5px"><?= $arrData['service_name_a'] ?></td>
   </tr>
   <tr>
     <td style="padding:5px">วันและช่วงเวลา</td>
     <td style="text-align:center;padding:5px">:</td>
     <td style="padding:5px"><?= $arrData['dateReserve'] ?></td>
   </tr>
 </table>
</div>
<div class="modal-footer">
  <input type="hidden"  name='tr_id' value="<?= $arrData['tr_id'] ?>">
  <input type="hidden"  name='action' value="CANCEL">
  <button type="button" class="btn btn-flat btn-default" style="width:100px;" data-dismiss="modal">ปิด</button>
  <button type="submit" class="btn btn-flat bg-navy" style="width:100px;">ยืนยัน</button>
</div>
