<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$mem_id     = $_SESSION["member"]['mem_id'];
$mem_name   = $_SESSION["member"]['mem_name'];
$mem_last   = $_SESSION["member"]['mem_last'];
$tel_no     = $_SESSION["member"]['tel_no'];
$email      = $_SESSION["member"]['email'];
$hide_list  = $_SESSION["member"]['hide_list'];

$datenow = date('Y-m-d');

$con = "";
$con2 = "r.date_reserve DESC,	r.time_start";

$status_reserve = $_POST['status_reserve'];

if($status_reserve == "R")
{
  $con = " and r.status_reserve	 = '$status_reserve' and r.date_reserve >= '$datenow'";
  $con2 = "r.date_reserve, r.time_start";
}else if($status_reserve == "S")
{
  $con = " and r.status_reserve	 = '$status_reserve' ";

}else if($status_reserve == "C")
{
  $con = " and r.status_reserve	 = '$status_reserve' ";
  //and r.date_reserve >= '$datenow'
}else if($status_reserve == "E")
{
  $con = " and (r.status_reserve	 not in ('S','C') and r.date_reserve < '$datenow')  or r.status_reserve	= 'E'";
  //and r.date_reserve >= '$datenow'
}

$sqls   = "SELECT r.*, a.agency_name	, s.service_name_a, TIMESTAMPDIFF(MINUTE, now() , cast(concat(r.date_reserve, ' ', r.time_end) as datetime)) as date_diff
           FROM t_reserve r
           LEFT JOIN t_agency a ON r.agency_code = a.agency_code
           LEFT JOIN t_service_agency s ON r.service_id = s.service_id and r.agency_code = s.agency_code
           where  r.mem_id = '$mem_id' $con
           ORDER BY $con2";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

if($dataCount > 0)
{
  foreach ($rows as $key => $value) {
    $agency_name = ltrim(str_replace("สำนักงานประกันสังคม","",$value['agency_name']));

    if($value['date_diff'] < 0 && $status_reserve == "R")
    {
        $tr_id = $value['tr_id'];
        $sql = "UPDATE t_reserve SET 	status_reserve = 'E' WHERE tr_id = '$tr_id'";
        DbQuery($sql,null);
        continue;
    };

    $dateReserve = convDatetoThaiMonth($value['date_reserve'])." ".$value['time_start']."-".$value['time_end']." น.";

    $arrData['agency_name'] = $agency_name;
    $arrData['ref_queue']   = $value['ref_queue'];
    $arrData['dateReserve'] = $dateReserve;
    $arrData['service_name_a'] = $value['service_name_a'];
    $arrData['tr_id'] = $value['tr_id'];
    $jsonData = json_encode($arrData);

    $dr = $value['date_reserve'];
    $statusReserve = $value['status_reserve'];

    $date1 = new DateTime($dr);
    $date2 = new DateTime($datenow);

    if($date1 < $date2)
    {
        $disable = "disabled";
    }else if($statusReserve != "R")
    {
       $disable = "disabled";
    }


    $ref_queue = $value['ref_queue'];


?>

<div class="col-md-4">
  <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="fa fa-tags"></i></span>
    <div class="info-box-content">
      <div>
        <span class="info-box-text">สำนักงานประกันสังคม</span>
        <span class="info-box-text2"><?= $agency_name;?></span>
      </div>
    </div>
    <table style="margin-left:10px;">
      <tr>
        <td style="width: 93px;">รหัสการจอง</td>
        <td style="width: 10px;text-align:center;">:</td>
        <td><div class="info-box-text3"><?= $ref_queue ?>
            <?php
                $agency_code = $value['agency_code'];
                if($ref_queue != "")
                {
                  if($value['status_reserve'] == "R")
                  {
                    echo "<a onclick=\"openQR('$agency_code','$ref_queue')\"><i class=\"fa fa-qrcode\"></i></a>";
                    $disable = "";
                  }
                }
            ?>
            </div>
        </td>
      </tr>
     <tr>
       <td style="width: 90px;">งานบริการ</td>
       <td style="width: 10px;text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $value['service_name_a'] ?></div></td>
     </tr>
     <tr>
       <td>วันและช่วงเวลา</td>
       <td style="text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $dateReserve ?></div></td>
     </tr>
   </table>
   <table style="width:100%;margin-top:10px;" class="">
    <tr>
      <td style="width: 33.33%;text-align:center;">
        <button type="button" class="btn btn-block btn-flat btn-default" style="border-top: 1px solid #ece6e6;"
        onclick="postURL('../reserve/?action=VIEW&tr_id=<?=$value['tr_id']?>')">
          <i class="fa fa-search icon-fa"></i> ดูรายการ
        </button>
      </td>
      <td style="text-align:center;">
        <button type="button" class="btn btn-block btn-flat btn-default <?=$disable ?>" onclick="postURL('../reserve/?action=EDIT&tr_id=<?=$value['tr_id']?>')"
          style="border-left: 1px solid #ece6e6;border-right: 1px solid #ece6e6;border-top: 1px solid #ece6e6;">
          <i class="ion ion-android-calendar icon-fa2"></i> เลื่อนคิว
        </button>
      </td>
      <td style="width: 33.33%;text-align:center;">
        <button type="button" class="btn btn-block btn-flat btn-default <?=$disable ?>" style="border-top: 1px solid #ece6e6;" onclick="cancelReserve('<?= urlencode($jsonData) ?>')">
          <i class="ion ion-android-cancel icon-fa2"></i> ยกเลิกคิว
        </button>
        <input type="hidden" id="statusReserve" value="<?= $status_reserve?>">
      </td>
    </tr>
  </table>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>

<?php } } ?>
