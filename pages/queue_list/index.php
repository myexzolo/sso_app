<!DOCTYPE html>
<?php
$pageName     = "ประวัติการจองคิว";
$pageCode     = "reserve list";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/reserve.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php

          $mem_id = @$_GET['mem_id'];
          if($mem_id != "" && !isset($_SESSION['member']['mem_name']))
          {
            $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
            //echo $sqls;
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true);
            $counts = $json['dataCount'];
            $rows   = $json['data'];
            if($counts == 1)
            {
              $_SESSION['member'] = $rows[0];
            }
          }

          $mem_name   = $_SESSION['member']['mem_name'];
          $mem_last   = $_SESSION['member']['mem_last'];

          include("../../inc/header.php");
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../home/"><i class="ion ion-android-home"></i> Home</a></li>
              <li class="active"><?= $pageCode ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <div class="row">
              <div class="col-md-4">

                <table style="width:100%;border: 1px solid #ece6e6">
                  <tr>
                    <td class="tableReserve">
                      <button type="button" id="btnReservR" class="btn btn-block btn-flat btnReserve" onclick="showList('R');" >จองคิว</button>
                    </td>
                    <td class="tableReserve">
                        <button type="button" id="btnReservS" class="btn btn-block btn-flat btnReserve tRBl" onclick="showList('S');" >สำเร็จ</button>
                    </td>
                    <td class="tableReserve">
                        <button type="button" id="btnReservC" class="btn btn-block btn-flat btnReserve tRBl" onclick="showList('C');">ยกเลิกคิว</button>
                    </td>
                    <td class="tableReserve">
                        <button type="button" id="btnReservE" class="btn btn-block btn-flat btnReserve tRBl" onclick="showList('E');">หมดเวลา</button>
                    </td>
                    <td class="tableReserve">
                        <button type="button" id="btnReserv" class="btn btn-block btn-flat btnReserve tRBl" onclick="showList('');">ทั้งหมด</button>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-md-12">
                <div style="padding: 5px;background-color: #eaeaead1;min-height:400px;border: 1px solid #ece6e6">
                  <div class="row" id="queue_list"></div>
                </div>
              </div>
            </div>
            <!-- Main row -->
              <!-- /.row -->
          </section>

          <div class="modal fade" id="myModalcencel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">

              <div class="modal-content">
                <div class="modal-header">
                    <h3 class="box-title" style="margin-top: 5px; margin-bottom: 5px;">
                      <i class="ion ion-android-cancel" style="color:red;"></i> ยืนยัน ยกเลิกการจองคิว
                    </h3>
                </div>
                <form id="formAED" novalidate enctype="multipart/form-data">
                <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form-cancel"></div>
                </form>
              </div>
            </div>
          </div>


          <div class="modal fade" id="myModalqr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">

              <div class="modal-content">
                <div class="modal-header">
                    <h3 class="box-title" style="margin-top: 5px; margin-bottom: 5px;">
                      <i class="fa fa-qrcode"></i> QR code
                    </h3>
                </div>
                    <div id="show-form-qr"></div>
              </div>
            </div>
          </div>


          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/reserve.js"></script>
    </body>
  </html>
