$(function ()
{
  $("#btnReservR").addClass('btnReserveFocus');
  showList("R");
});

function showList(status_reserve){
  if(status_reserve == "R"){
    $("#btnReservR").addClass('btnReserveFocus');
    $("#btnReservS").removeClass('btnReserveFocus');
    $("#btnReservC").removeClass('btnReserveFocus');
    $("#btnReservE").removeClass('btnReserveFocus');
    $("#btnReserv").removeClass('btnReserveFocus');
  }else if(status_reserve == "S")
  {
    $("#btnReservR").removeClass('btnReserveFocus');
    $("#btnReservS").addClass('btnReserveFocus');
    $("#btnReservC").removeClass('btnReserveFocus');
    $("#btnReservE").removeClass('btnReserveFocus');
    $("#btnReserv").removeClass('btnReserveFocus');
  }else if(status_reserve == "C")
  {
    $("#btnReservR").removeClass('btnReserveFocus');
    $("#btnReservS").removeClass('btnReserveFocus');
    $("#btnReservC").addClass('btnReserveFocus');
    $("#btnReservE").removeClass('btnReserveFocus');
    $("#btnReserv").removeClass('btnReserveFocus');
  }else if(status_reserve == "E")
  {
    $("#btnReservR").removeClass('btnReserveFocus');
    $("#btnReservS").removeClass('btnReserveFocus');
    $("#btnReservC").removeClass('btnReserveFocus');
    $("#btnReservE").addClass('btnReserveFocus');
    $("#btnReserv").removeClass('btnReserveFocus');
  }else
  {
    $("#btnReservR").removeClass('btnReserveFocus');
    $("#btnReservS").removeClass('btnReserveFocus');
    $("#btnReservC").removeClass('btnReserveFocus');
    $("#btnReservE").removeClass('btnReserveFocus');
    $("#btnReserv").addClass('btnReserveFocus');
  }
  $("#statusReserve").val(status_reserve);

  $.post("ajax/list.php",{status_reserve:status_reserve})
    .done(function( data ) {
      $('#queue_list').html(data);
  });
}


function cancelReserve(jsonData)
{
  $.post("ajax/confrimCancel.php",{jsonData:jsonData})
    .done(function( data ) {
      $('#show-form-cancel').html(data);
      $('#myModalcencel').modal({backdrop:'static'});
  });
}

function openQR(agency_code,ref_queue)
{
  $.post("ajax/genQR.php",{agency_code:agency_code,ref_queue:ref_queue})
    .done(function( data ) {
      $('#show-form-qr').html(data);
      $('#myModalqr').modal({backdrop:'static'});

  });
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  //console.log("formAED");
  $('#myModalcencel').modal('toggle');
  var statusReserve = $("#statusReserve").val();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        console.log(">>>"+statusReserve);
        showList(statusReserve);
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
