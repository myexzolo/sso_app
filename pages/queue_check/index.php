<!DOCTYPE html>
<?php
$pageName     = "ตรวจสอบคิว";
$pageCode     = "Queue";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/queue_check.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php
          $mem_id = @$_GET['mem_id'];
          if($mem_id != "" && !isset($_SESSION['member']['mem_name']))
          {
            $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
            //echo $sqls;
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true);
            $counts = $json['dataCount'];
            $rows   = $json['data'];
            if($counts == 1)
            {
              $_SESSION['member'] = $rows[0];
            }
          }

          $mem_name   = $_SESSION['member']['mem_name'];
          $mem_last   = $_SESSION['member']['mem_last'];
          $mem_id     = $_SESSION['member']['mem_id'];


          include("../../inc/header.php");
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../home/"><i class="ion ion-android-home"></i> Home</a></li>
              <li class="active"><?= $pageCode ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <div class="row">
              <div class="col-md-12">
                <div style="padding: 5px;background-color: #eaeaead1;min-height:400px;border: 1px solid #ece6e6">
                  <input type="hidden" id="mem_id" value="<?= $mem_id ?>">
                  <div class="row" id="queue_list"></div>
                </div>
              </div>
            </div>
            <!-- Main row -->
              <!-- /.row -->
          </section>

          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/queue_check.js"></script>
    </body>
  </html>
