<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$mem_id     = $_POST['mem_id'];

$dateStart = date('Y-m-d')." 00:00:00";
$dateEnd  = date('Y-m-d')." 23:59:59";


$sqls   = "SELECT q.trans_queue_id,q.queue_code,q.ref_code,q.date_start,q.type_service,q.menu_id,ser.service_code_a,q.point_id,q.service_id,
                  ser.service_name_a,ser.kpi_time_a,q.service_channel,a.agency_name,a.agency_code
           FROM t_trans_queue q
           LEFT JOIN t_agency a ON q.agency_code = a.agency_code
           LEFT JOIN t_service_agency ser ON q.service_id = ser.service_id and q.agency_code = ser.agency_code
           WHERE q.status_queue = 'W'
           AND q.date_start between '$dateStart' and '$dateEnd'
           AND q.member_id = $mem_id
           ORDER BY date_start";

//echo $sqls;
$querys = DbQuery($sqls,null);
$json   = json_decode($querys, true);
$dataCount  = $json['dataCount'];
$errorInfo  = $json['errorInfo'];
$rows       = $json['data'];
//echo $sqls;

if($dataCount > 0)
{
  foreach ($rows as $key => $value) {
    $agency_name  = ltrim(str_replace("สำนักงานประกันสังคม","",$value['agency_name']));
    $agency_code  = $value['agency_code'];
    $queue_code   = $value['queue_code'];
    $service_code = $value['service_code_a'];
    $date_start   = $value['date_start'];
    $point_id     = $value['point_id'];
    $service_id   = $value['service_id'];

    $sqlr = "SELECT count(trans_queue_id) as num
            FROM t_trans_queue
            WHERE date_start between '$dateStart' and '$dateEnd' and date_start < '$date_start' and status_queue = 'W'
            and queue_code LIKE '$service_code%' and queue_code <> '$queue_code' and agency_code = '$agency_code'";

    //echo   $sqlr;
    $queryr  = DbQuery($sqlr,null);
    $num    = json_decode($queryr,true)['data'][0]['num'];



    $sqls = "SELECT *
            FROM t_service_channel
            WHERE is_active = 'Y' and agency_code = '$agency_code' and point_id = '$point_id' order by service_channel";
    $querys = DbQuery($sqls,null);
    $jsons  = json_decode($querys,true);
    $dataCount = $jsons['dataCount'];

    $channel = "";
    $channel_tmp = 0;
    if($dataCount > 0)
    {
      foreach ($jsons['data'] as $values) {

        $service_id_list = $values['service_id_list'];
        $service_channel = $values['service_channel'];
        $serviceArr      = explode(",",$service_id_list);


        if(in_array($service_id, $serviceArr))
        {
          if($channel == "")
          {
            $channel       = $service_channel;
            $channel_tmp   = $service_channel;
            $channel_start = $service_channel;
          }
          else
          {
            $numC = intval($channel_tmp) + 1;

            if($service_channel == $numC)
            {
               $channel   = $channel_start." - ".$service_channel;
            }
            else
            {
              $channel  .=  ", ".$service_channel;
            }
          }
          $channel_tmp = $service_channel;
        }
      }
    }


    $date_start = DateTimeThai($value['date_start']);

?>

<div class="col-md-4">
  <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="fa fa-ticket"></i></span>
    <div class="info-box-content">
      <div>
        <span class="info-box-text">สำนักงานประกันสังคม</span>
        <span class="info-box-text2"><?= $agency_name;?></span>
      </div>
    </div>
    <table style="margin-left:10px;">
      <tr>
        <td style="width: 108px;">รหัสบริการ</td>
        <td style="width: 10px;text-align:center;">:</td>
        <td><div class="info-box-text3"><b><?= $queue_code ?><b></div>
        </td>
      </tr>
     <tr>
       <td>งานบริการ</td>
       <td style="width: 10px;text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $value['service_name_a'] ?></div></td>
     </tr>
     <tr>
       <td>วันที่เข้ารับบริการ</td>
       <td style="text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $date_start ?></div></td>
     </tr>
     <tr>
       <td>ช่องบริการ</td>
       <td style="text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $channel ?></div></td>
     </tr>
     <tr>
       <td>คิวก่อนหน้า</td>
       <td style="text-align:center;">:</td>
       <td><div class="info-box-text3"><?= $num ?> คิว</div></td>
     </tr>
   </table>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>

<?php } } ?>
