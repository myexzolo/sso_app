$(function ()
{
  showList();
  setInterval(function(){ showList(); }, 30000);
});

function showList(){
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  var mem_id = $('#mem_id').val();
  $.post("ajax/list.php",{mem_id:mem_id})
    .done(function( data ) {
      $('#queue_list').html(data);
  });
}
