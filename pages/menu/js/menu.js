$(function ()
{
  showMainMenu();
});

function showMainMenu(){
  var menu = $("#menu").val();
  var code = $("#code").val();
  var bcode = $("#bcode").val();

  $.post("ajax/main_menu.php",{menu:menu,code:code,bcode:bcode})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function goSubmenu(service_id_list,agency_code,point_id,code,menuList)
{
  $.post("ajax/sub_menu.php",{sil:service_id_list,bcode:agency_code,pid:point_id,code:code,m:menuList})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function showModal(service_id,agency_code,service_code,point_id,service_name)
{
  var agency_name = $('#agencyName').val();
  var mem_id      = $('#mem_id').val();
  console.log(mem_id);
  $.post("ajax/formService.php",{
    service_id:service_id,
    agency_code:agency_code,
    service_code:service_code,
    point_id:point_id,
    service_name:service_name,
    agency_name:agency_name,
    mem_id:mem_id
  }).done(function( data ) {
      $('#show-form-service').html(data);
      $('#myModal').modal('toggle');
  });
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'service/saveQueue.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      setTimeout(function(){
        $('#myModal').modal('toggle');
        gotoPage('../queue_check/');
      }, 1000);
    });
  }
});
