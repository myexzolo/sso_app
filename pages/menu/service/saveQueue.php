<?php

include '../../../inc/function/connect.php';
include '../../../inc/function/mainFunc.php';


$service_id   = $_POST['service_id'];
$agency_code  = $_POST['agency_code'];
$service_code = $_POST['service_code'];
$point_id     = $_POST['point_id'];
$member_id     = $_POST['member_id'];


$date = date('Y-m-d');
$queue_code  = getCode($agency_code,$date,$service_code,3);
$ref_queue   = randomString($length = 8 , $type = 2);
$arr['queue_code']    = $queue_code;
$arr['date_start']    = date('Y-m-d H:i:s');
$arr['agency_code']   = $agency_code;
$arr['type_service']  = 2;//1 = เมนู 2=บริการ
$arr['service_id']    = $service_id;
$arr['status_queue']  = 'W';//W = รอรับบริการ S = รับบริการ E = จบงาน C = ยกเลิกคิว H= Hold คิว
$arr['type_queue']    = 1;//ประเภทคิว 1=จากตู้ , 2 = จากจองคิว
$arr['point_id']      = $point_id;
$arr["ref_queue"]     = $ref_queue;
$arr["member_id"]     = $member_id;


$sql   = DBInsertPOST($arr,'t_trans_queue');
$query = DbQuery($sql,null);
$row   = json_decode($query,true);
if(intval($row['errorInfo'][0]) == 0){

  $trans_queue_id = $row['id'];

  $sqls   = "SELECT * FROM t_trans_queue where trans_queue_id = '$trans_queue_id'"; //เปลี่ยน table
  $querys     = DbQuery($sqls,null);
  $json       = json_decode($querys, true);
  $rows       = $json['data'][0];

  $rows["action"]   = "ADD";
  $rows["ref_code"] = $agency_code.'#C'.$trans_queue_id;
  $agency_code      = $rows["agency_code"];

  $rows = chkDataEmpty($rows);

  $ipAgency    = getIPbyAgency($agency_code);
  $data_array  = array(
                     "functionName" => "manageTransQueue",  ///แก้ ชื่อ Service
                     "dataJson" => $rows,
                   );
  $url        = "http://$ipAgency/ws/service.php";

  $make_call = callAPI('POST', $url, json_encode($data_array));
  $response = json_decode($make_call, true);
  $status   = $response['status'];
  $data     = $response['data'];

  $arrUpdate['trans_queue_id']     =  $trans_queue_id; //แก้ ID
  $arrUpdate['ref_code']           =  $rows["ref_code"];
  $arrUpdate['status_send']        =  "N";

  $sql = "";
  if($status == "200")
  {
      $arrUpdate['status_send']  =  "S";
  }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
      $arrLog['table_name'] = 't_trans_queue';
      $arrLog['id_update']  = 'trans_queue_id';
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service');
  }
  $sql .= DBUpdatePOST($arrUpdate,'t_trans_queue','trans_queue_id');
  //echo $sql;
  DbQuery($sql,null);

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'สำเร็จ', 'id' => $trans_queue_id, 'service_code' => $service_code, 'queue_code' => $queue_code, 'ref_queue' => $ref_queue)));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'fail')));
}


?>
