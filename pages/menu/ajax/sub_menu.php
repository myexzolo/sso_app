<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$bg         = "background-color:#fff;";
$service_id_list  = @$_POST['sil'];
$agency_code      = @$_POST['bcode'];
$point_id         = @$_POST['pid'];
$language         = @$_POST['language'];
$code             = @$_POST['code'];
$menu             = @$_POST['m'];

$sqlc = "SELECT s.*
         FROM t_service_agency s
         where s.is_active = 'Y' and s.agency_code = '$agency_code' and s.service_id IN ($service_id_list)
         ORDER BY s.service_code_a";

//echo   $sqlc;

$queryc = DbQuery($sqlc,null);
$jsonc = json_decode($queryc,true);
?>
<div class="row" style="margin-left:10px;margin-right:10px;">
<?php
foreach ($jsonc['data'] as $valuec) {

  $service_id      = $valuec['service_id'];
  $service_code_a  = $valuec['service_code_a'];
  $kpi_time_a      = $valuec['kpi_time_a'];
  $service_name    = $valuec['service_name_a'];
  $kpi_time        = $valuec['kpi_time_a'];


  if($language == "en")
  {
    $service_name = $valuec['service_name_en_a'];
  }

  $link = "showModal('$service_id','$agency_code','$service_code_a','$point_id','$service_name')";

?>
<div class="col-md-6">
  <div style="margin-top:15px;">
    <button type="button" class="btn btn-default btn-block btn-flat" onclick="<?=$link?>" style="font-size:20px;height:40px;"><?=$service_name?></button>
  </div>
</div>
<?php
}
?>
</div>
  <div style="margin:15px;position:absolute;bottom: 3px;right: 10px;" align="center">
    <button type="button" class="btn btn-info btn-block btn-flat" onclick="showMainMenu()"
    style="font-size:20px;height:40px;width:100px;">กลับ</button>
  </div>
