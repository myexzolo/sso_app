<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$bg         = "background-color:#fff;";
$menuList     = @$_POST['menu'];
$code         = @$_POST['code'];
$agency_code  = @$_POST['bcode'];


$sqlc = "SELECT m.*
         FROM t_menu m
         where m.is_active = 'Y' and m.agency_code = '$agency_code' and m.menu_id IN ($menuList)
         ORDER BY m.menu_code , m.start_time";

//echo   $sqlc;

$queryc = DbQuery($sqlc,null);
$jsonc = json_decode($queryc,true);
?>
<div class="row" style="margin-left:10px;margin-right:10px;">
<?php
foreach ($jsonc['data'] as $valuec)
{

  $start_time   = $valuec['start_time'];
  $end_time     = $valuec['end_time'];
  $type_service = $valuec['type_service'];
  $menu_id      = $valuec['menu_id'];
  $menu_code    = $valuec['menu_code'];
  $menu_name    = $valuec['menu_name'];
  $point_id     = $valuec['point_id'];

  $ds = '2020-01-01 '.$start_time.":00";
  $de = '2020-01-01 '.$end_time.":00";
  $dn = '2020-01-01 '.date('H:i:s');

  //echo   $ds.", ".$de.", ".$dn."<br>";

  $dteStart = new DateTime($ds);
  $dteEnd   = new DateTime($de);
  $dteNow   = new DateTime($dn);

  $dteStart = $dteStart->getTimestamp();
  $dteEnd   = $dteEnd->getTimestamp();
  $dteNow   = $dteNow->getTimestamp();

  //echo $dteStart.", ".$dteEnd.", ".$dteNow."<br />";
  if($dteNow >= $dteStart && $dteNow <= $dteEnd)
  {
    $link = "";
    $service_id_list  = $valuec['service_id_list'];
    if($type_service == '1')
    {
        $link = "goSave('$menu_id','$agency_code','$menu_code','$point_id','$menu_name')";
    }else{
        $serviceArr = explode(",",$service_id_list);
        if(count($serviceArr) == 1)
        {

          $sqlc = "SELECT s.*
                   FROM t_service_agency s
                   where s.is_active = 'Y' and s.agency_code = '$agency_code' and s.service_id = $serviceArr[0]";


         $queryc = DbQuery($sqlc,null);
         $jsonc = json_decode($queryc,true);
         $rowc  = $jsonc['data'][0];

         $service_id      = $rowc['service_id'];
         $service_code_a  = $rowc['service_code_a'];
         $service_name_a  = $rowc['service_name_a'];
         $kpi_time        = $rowc['kpi_time_a'];

          $link = "showModal('$service_id','$agency_code','$service_code_a','$point_id','$service_name_a')";
        }else{
          $link = "goSubmenu('$service_id_list','$agency_code','$point_id','$code','$menuList')";
        }
    }
  ?>
  <div class="col-md-6">
    <div style="margin-top:15px;">
      <button type="button" class="btn btn-default btn-block btn-flat" onclick="<?=$link?>" style="font-size:20px;height:40px;"><?=$valuec['menu_name']?></button>
    </div>
  </div>
<?php
  }
}
?>
</div>
