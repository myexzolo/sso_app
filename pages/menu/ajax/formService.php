<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$bg           = "background-color:#fff;";
$service_id   = @$_POST['service_id'];
$agency_code  = @$_POST['agency_code'];
$service_code = @$_POST['service_code'];
$point_id     = @$_POST['point_id'];
$agency_name  = @$_POST['agency_name'];
$service_name = @$_POST['service_name'];
$mem_id       = @$_POST['mem_id'];

?>
<div class="modal-body">
   <div class="info-box">
     <span class="info-box-icon bg-aqua" style="height: 80px;"><i class="fa fa-ticket"></i></span>
     <div class="info-box-content">
       <div>
         <span class="info-box-text">สำนักงานประกันสังคม</span>
         <span class="info-box-text2"><?=$agency_name;?></span>
         <input type="hidden" value="<?= $service_id?>" name="service_id">
         <input type="hidden" value="<?= $agency_code?>" name="agency_code">
         <input type="hidden" value="<?= $service_code?>" name="service_code">
         <input type="hidden" value="<?= $point_id?>" name="point_id">
         <input type="hidden" value="<?= $mem_id?>" name="member_id">
       </div>
     </div>
     <table style="margin:5px 10px 10px 0px;">
      <tr>
        <td style="width: 90px;padding:5px 0px 5px 0px">งานบริการ</td>
        <td style="width: 20px;text-align:center;">:</td>
        <td><div class="info-box-text3"><?= $service_name ?></div></td>
      </tr>
    </table>
   </div>
 </div>
 <div class="modal-footer">
   <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
   <button type="submit" class="btn btn-primary btn-flat" style="width:100px;" >ยืนยัน</button>
 </div>
