<!DOCTYPE html>
<?php
$pageName     = "ทำรายการ";
$pageCode     = "Menu";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/menu.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php
          $code   = @$_GET['code'];
          $mem_id = @$_GET['mem_id'];

          if($mem_id != "" && !isset($_SESSION['member']['mem_name']))
          {
            $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
            //echo $sqls;
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true);
            $counts = $json['dataCount'];
            $rows   = $json['data'];
            if($counts == 1)
            {
              $_SESSION['member'] = $rows[0];
            }
          }

          include("../../inc/header.php");
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');

          if($code != "")
          {
            $codeArr = explode("_", $code);

            $code         = $codeArr[0];
            $agency_code  = $codeArr[1];

            $sql = "SELECT k.*, a.agency_name
                    FROM t_kiosk k, t_agency a
                    WHERE k.kiosk_code = '$code' and k.agency_code = '$agency_code' and k.agency_code = a.agency_code";


            $query      = DbQuery($sql,null);
            $json       = json_decode($query, true);
            $errorInfo  = $json['errorInfo'];
            $dataCount  = $json['dataCount'];

            if($dataCount > 0)
            {
              $data  =   $json['data'][0];

              $service_open =  $data['service_open'];
              $service_close = $data['service_close'];
              $point_id      = $data['point_id'];
              $agency_name   = ltrim(str_replace("สำนักงานประกันสังคม","",$data['agency_name']));
              $codeTmp = $code;

              $menu_id_list = $data['menu_id_list'];
              if($menu_id_list != "")
              {
                $menu_id_list = "'".str_replace(",","','",$menu_id_list)."'";

                $sqlc = "SELECT * FROM t_menu WHERE ref_code IN ($menu_id_list) and is_active = 'Y'";
                $queryc = DbQuery($sqlc,null);
                $jsonc = json_decode($queryc,true);

                $menuIdlist = "";
                foreach ($jsonc['data'] as $valuec)
                {
                   $menu_id     = $valuec['menu_id'];
                   $type_menu   = $valuec['type_menu'];
                   $start_time  = $valuec['start_time'];
                   $end_time    = $valuec['end_time'];

                   $time = $start_time."|".$end_time;

                   if($type_menu == 2)
                   {
                     echo "<input type=\"hidden\" class=\"break\" value=\"$time\">";
                   }else{
                     if($menuIdlist == "")
                     {
                       $menuIdlist = $menu_id;
                     }else
                     {
                       $menuIdlist .= ",".$menu_id;
                     }
                   }
                }
              }
            }else{
              $menuIdlist = "";
              $codeTmp = "";
            }
          }
        ?>
        <input type="hidden" id="menu" value="<?= $menuIdlist ?>">
        <input type="hidden" id="code" value="<?= $code ?>">
        <input type="hidden" id="bcode" value="<?= $agency_code ?>">
        <input type="hidden" id="agencyName" value="<?= $agency_name ?>">
        <input type="hidden" id="mem_id" value="<?= $mem_id ?>">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1><?= $pageName ?></h1>
            <ol class="breadcrumb">
              <li><a href="../home/"><i class="ion ion-android-home"></i> Home</a></li>
              <li class="active"><?= $pageCode ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row" >
              <div class="reg-box">
                <!-- /.login-logo -->
                <div class="col-md-8 col-md-offset-2">
                  <div class="box box-solid" style="min-height:450px;padding-bottom: 80px;">
                    <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                      <div id="show-form"></div>
                  </div>
                  <!-- /.login-box-body -->
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">
                <div class="modal-content">
                  <div class="modal-header">
                    ยืนยันการทำรายการ
                  </div>
                  <form id="formAED" novalidate enctype="multipart/form-data">
                  <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form-service"></div>
                  </form>

                </div>
              </div>
            </div>
              <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/menu.js"></script>
    </body>
  </html>
