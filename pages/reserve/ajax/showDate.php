<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$bg         = "background-color:#fff;";
?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">
    <ul class="nav nav-stacked">
    <?php
    $dateNow = date('Y-m-d');

    $date[0] = $dateNow;
    $date[1] = date('Y-m-d',strtotime($dateNow . "+1 days"));
    $date[2] = date('Y-m-d',strtotime($dateNow . "+2 days"));
    $date[3] = date('Y-m-d',strtotime($dateNow . "+3 days"));
    $date[4] = date('Y-m-d',strtotime($dateNow . "+4 days"));
    $date[5] = date('Y-m-d',strtotime($dateNow . "+5 days"));
    $date[6] = date('Y-m-d',strtotime($dateNow . "+6 days"));
    $date[7] = date('Y-m-d',strtotime($dateNow . "+7 days"));
    $date[8] = date('Y-m-d',strtotime($dateNow . "+8 days"));
    $date[9] = date('Y-m-d',strtotime($dateNow . "+9 days"));
    $date[10] = date('Y-m-d',strtotime($dateNow . "+10 days"));


    foreach ($date as $value) {

        $dateTHFull = datetoThaiFull($value);
    ?>
    <li align="center"><a href="#" onclick="setDate('<?= $value ?>','<?= $dateTHFull ?>')"><?= $dateTHFull ?></a></li>
    <?php
    }
    ?>
    </ul>
</div>
<div class="modal-footer">
  <input type="hidden" id='dateReserve'>
  <input type="hidden" id='dateThReserve'>
  <button type="button" class="btn btn-flat bg-navy btn-block" onclick="setDataDate()">ตกลง</button>
</div>
