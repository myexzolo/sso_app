<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$bg         = "background-color:#fff;";
?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">
    <ul class="nav nav-stacked">
    <?php
    $agency_code = $_POST['agency_code'];


    $sqls   = "SELECT *
               FROM t_service_agency
               where is_active = 'Y' and agency_code = '$agency_code' and reserve_status_a = 'Y';
               ORDER BY cat_service_id, service_name_a";

    //echo $sqls;
    $querys     = DbQuery($sqls,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0)
    {
      foreach ($rows as $key => $value) {
        $is_active        = $value['is_active'];
        $service_name_a   = $value['service_name_a'];
        $service_id       = $value['service_id'];
        $check_list       = urlencode($value['check_list_a']);
    ?>
    <li><a href="#" onclick="setServiceId('<?= $service_id ?>','<?= $service_name_a ?>','<?=$check_list?>')"><?= $service_name_a ?></a></li>
    <?php
      }
    }
    ?>
    </ul>
</div>
<div class="modal-footer">
  <input type="hidden" id='serviceId'>
  <input type="hidden" id='serviceName'>
  <input type="hidden" id='checkList'>
  <button type="button" class="btn btn-flat bg-navy btn-block" onclick="setDataService()">ตกลง</button>
</div>
