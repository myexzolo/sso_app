<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
require_once '../../../phpqrcode/qrlib.php';

$action       = isset($_POST['action'])?$_POST['action']:"";
$tr_id        = isset($_POST['tr_id'])?$_POST['tr_id']:"";


//print_r($_POST);
unset($_POST["action"]);

if($action == "ADD")
{
    unset($_POST["tr_id"]);

    $ref_queue                = randomString($length = 8 , $type = 2);
    $_POST["date_create"]     = date('Y/m/d H:i:s');
    $_POST["status_reserve"]  = "R";
    $_POST["ref_queue"]       = $ref_queue;
    $sql = DBInsertPOST($_POST,'t_reserve');
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_reserve','tr_id');
}
else if($action == "CANCEL")
{
    $sql = "UPDATE t_reserve SET status_reserve = 'C' WHERE tr_id = '$tr_id'";
}
else if($action == "DEL")
{
    $sql = "UPDATE t_reserve SET is_active = 'D' WHERE tr_id = '$tr_id'";
}

//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0)
{
  if($action == "ADD")
  {
    $tr_id = $row['id']; ///แก้ ID
  }

  $sqls       = "SELECT * FROM t_reserve where tr_id = '$tr_id'"; //เปลี่ยน table
  $querys     = DbQuery($sqls,null);
  $json       = json_decode($querys, true);
  $rows       = $json['data'][0];

  $rows["action"]   = "U";
  $agency_code      = $rows["agency_code"];

  if($action == 'ADD')
  {
    $rows["ref_code"] = $agency_code."#C".$tr_id;   ///แก้ ID
    $rows["action"]   = "ADD";

    $text = $agency_code."#".$ref_queue;

    $file = "../../../qr/".$ref_queue.".png";
    $ecc = 'H';
    $pixel_size = 128;
    $frame_size = 1;

    QRcode::png($text, $file, $ecc, $pixel_size, $frame_size);
  }

  $rows = chkDataEmpty($rows);

  $ipAgency    = getIPbyAgency($agency_code);
  $data_array  = array(
                     "functionName" => "manageReserve",  ///แก้ ชื่อ Service
                     "dataJson" => $rows,
                   );

  $url  = "http://$ipAgency/ws/service.php";

  // echo $url;
  //echo json_encode($data_array);

  $make_call = callAPI('POST', $url, json_encode($data_array));
  $response = json_decode($make_call, true);
  $status   = $response['status'];
  $data     = $response['data'];

  $arrUpdate['tr_id']        =  $tr_id; //แก้ ID
  $arrUpdate['ref_code']     =  $rows["ref_code"];
  $arrUpdate['status_send']  =  "N";

  // print_r($response);
  $sql = "";
  if($status == "200")
  {
      $arrUpdate['status_send']  =  "S";
      ///แก้ ชื่อ table
  }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array);
      $arrLog['table_name']   = 't_reserve';
      $arrLog['id_update']    = 'tr_id';
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
  }

  $sql .= DBUpdatePOST($arrUpdate,'t_reserve','tr_id');
  DbQuery($sql,null);


  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
