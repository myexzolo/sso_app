<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$bg          = "background-color:#fff;";

$dateReserve  = $_POST['dateReserve'];
$dateNow      = date("j");
$dateR        = date("j", strtotime($_POST['dateR']));

$timeNow  = "";
if($dateNow == $dateR)
{
  $timeNow   = date("G") + 2;
}
//echo $dateNow.", ".$dateR.", ".$timeNow;

?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">
    <ul class="nav nav-stacked">
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 8 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('07:00 - 08:00','07:00','08:00')">07:00 - 08:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 9 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList"  onclick="setTime('08:00 - 09:00','08:00','09:00')">08:00 - 09:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 10 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('09:00 - 10:00','09:00','10:00')">09:00 - 10:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 11 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('10:00 - 11:00','10:00','11:00')">10:00 - 11:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 12 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('11:00 - 12:00','11:00','12:00')">11:00 - 12:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 13 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('12:00 - 13:00','12:00','13:00')">12:00 - 13:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 14 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('13:00 - 14:00','13:00','14:00')">13:00 - 14:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 15 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('14:00 - 15:00','14:00','15:00')">14:00 - 15:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 16 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('15:00 - 16:00','15:00','16:00')">15:00 - 16:00</button>
      </li>
      <li align="center" class="<?= $timeNow != "" & intval($timeNow) >= 17 ? "noDisplay" : "" ?>">
        <button type="button" class="btntimeList" onclick="setTime('16:00 - 17:00','16:00','17:00')">16:00 - 17:00</button>
      </li>
    </ul>
</div>
<div class="modal-footer">
  <input type="hidden" id='dateReserve' value="<?=$dateReserve ?>">
  <input type="hidden" id='timeReserve'>
  <input type="hidden" id='timeStart'>
  <input type="hidden" id='timeEnd'>
  <button type="button" class="btn btn-flat bg-navy btn-block" onclick="setDatatime()">ตกลง</button>
</div>
