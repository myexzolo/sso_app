<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$bg          = "background-color:#fff;";

$dateReserve  = $_POST['dateReserve'];
$dateNow      = date("j");
$dateR        = date("j", strtotime($_POST['dateR']));

$timeNow  = "";
if($dateNow == $dateR)
{
  $timeNow   = date("G") + 2;
}
//echo $dateNow.", ".$dateR.", ".$timeNow;

?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">
    <div align="center" style="margin: 40px 15px 25px 15px;">
    <table style="width:360px">
      <tr>
        <td style="width: 120px;text-align:right;padding:10px">เวลาเริ่มจอง</td>
        <td style="text-align:center;padding:10px">
          <select style="width:100%" class="form-control" id="ts1">
            <?php
              for($x=6; $x<= 18; $x++)
              {
                  $val = str_pad($x, 2, '0', STR_PAD_LEFT);
            ?>
                <option value="<?=$val?>"><?=$val?></option>
            <?php
              }
            ?>
          </select>
        </td>
        <td style="width: 10px;text-align:center;padding:10px">:</td>
        <td style="text-align:center;padding:10px">
        <select style="width:100%" class="form-control" id="ts2">
          <?php
            for($x=0; $x<= 11; $x++)
            {
                $num = ($x * 5);
                $val = str_pad($num, 2, '0', STR_PAD_LEFT);
          ?>
              <option value="<?=$val?>"><?=$val?></option>
          <?php
            }
          ?>
        </select>
        </td>
      </tr>
    </table>
    <table style="width:360px">
      <tr>
        <td style="width: 120px;text-align:right;padding:10px">เวลาสิ้นสุดจอง</td>
        <td style="text-align:center;padding:10px">
          <select style="width:100%" class="form-control" id="te1">
            <?php
              for($x=6; $x<= 18; $x++)
              {
                  $val = str_pad($x, 2, '0', STR_PAD_LEFT);
            ?>
                <option value="<?=$val?>"><?=$val?></option>
            <?php
              }
            ?>
          </select>
        </td>
        <td style="width: 10px;text-align:center;padding:10px">:</td>
        <td style="text-align:center;padding:10px">
        <select style="width:100%" class="form-control" id="te2">
          <?php
            for($x=0; $x<= 11; $x++)
            {
                $num = ($x * 5);
                $val = str_pad($num, 2, '0', STR_PAD_LEFT);
          ?>
              <option value="<?=$val?>"><?=$val?></option>
          <?php
            }
          ?>
        </select>
        </td>
      </tr>
    </table>
  </div>
</div>
<div class="modal-footer">
  <input type="hidden" id='dateReserve' value="<?=$dateReserve ?>">
  <input type="hidden" id='timeReserve'>
  <input type="hidden" id='timeStart'>
  <input type="hidden" id='timeEnd'>
  <button type="button" class="btn btn-flat bg-navy btn-block" onclick="setDatatime2()">ตกลง</button>
</div>
