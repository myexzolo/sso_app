<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$bg         = "background-color:#fff;";
$tr_id   = @$_POST['tr_id'];
$action  = @$_POST['action'];

$disabled = "";
$disabled2 = "";

if($action == "VIEW")
{
  $disabled = "disabled";
}else if($action == "EDIT")
{
  $disabled2 = "disabled";
}else{
  $action = "ADD";
}

if($tr_id != "")
{

  $sql   = "SELECT r.*, a.agency_name	, s.service_name_a, s.check_list_a
            FROM t_reserve r, t_agency a , t_service_agency s
            where r.tr_id = '$tr_id' and a.agency_code = s.agency_code
                  and r.agency_code = a.agency_code and r.service_id = s.service_id";


  $query     = DbQuery($sql,null);
  $json      = json_decode($query, true);
  $row       = $json['data'][0];

  $mem_id     = $row['mem_id'];
  $mem_name   = $row['mem_name'];
  $mem_last   = $row['mem_last'];
  $tel_no     = $row['tel_no'];
  $email      = $row['email'];
  $hide_list  = $row['hide_list'];
  $date_reserve   = $row['date_reserve'];
  $time_start     = $row['time_start'];
  $time_end       = $row['time_end'];
  $service_id     = $row['service_id'];
  $service_name   = $row['service_name_a'];
  $agency_code    = $row['agency_code'];
  $check_list     = $row['check_list_a'];
  $agency_name    = ltrim(str_replace("สำนักงานประกันสังคม","",$row['agency_name']));

  $dateReserve    = datetoThaiFull($date_reserve)." ".$time_start." - ".$time_end;


}else{
  $mem_id = $_SESSION["member"]['mem_id'];
  $mem_name = $_SESSION["member"]['mem_name'];
  $mem_last = $_SESSION["member"]['mem_last'];
  $tel_no = $_SESSION["member"]['tel_no'];
  $email = $_SESSION["member"]['email'];
  $hide_list = $_SESSION["member"]['hide_list'];

  $check_list     = "";
  $agency_code    = "";
  $date_reserve   = "";
  $time_start     = "";
  $time_end       = "";
  $agency_name    = "-- กรุณาเลือก --";
  $service_name   = "-- กรุณาเลือก --";
  $dateReserve    = "-- กรุณาเลือก --";
}


?>
<div class="box-body">
  <input type="hidden" value="<?= $action?>" name="action" class="form-control" required>
  <input type="hidden" value="<?=$tr_id?>" name="tr_id" class="form-control" >
  <div class="row">
    <div class="col-md-12">
      <h2 class="page-header">
        <i class="ion ion-android-calendar"></i> รายละเอียดการจอง
      </h2>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ชื่อ</label>
        <input type="hidden" value="<?= $mem_id ?>" name="mem_id" class="form-control" required >
        <input type="hidden" value="<?= $hide_list ?>" name="hide_list" class="form-control">
        <input type="text" value="<?= $mem_name?>" name="mem_name" class="form-control" required <?=$disabled." ".$disabled2 ?>>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>นามสกุล</label>
        <input type="text" value="<?= $mem_last?>" name="mem_last" class="form-control" required <?=$disabled." ".$disabled2 ?>>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>เบอร์โทร</label>
        <input type="tel" value="<?=$tel_no ?>" name="tel_no" class="form-control" required <?=$disabled." ".$disabled2 ?>>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Email</label>
        <input type="email" value="<?= $email ?>" name="email" class="form-control" required <?=$disabled." ".$disabled2 ?>>
      </div>
    </div>
    <div class="col-md-12">
       <label>กรุณาเลือก</label>
       <p>เมื่อจองแล้วกรุณามาก่อนเวลาอย่างน้อย 30 นาที</p>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="txtDate <?=$disabled." ".$disabled2?>" id="boxAgency" onclick="showAgency('')" class="form-control">
          <div class="pull-left" style="width:80%">
            <span>สำนักงานประกันสังคม สาขา</span>
            <div class="txtSelect" id="dateSelectAgency"><?= $agency_name ?></div>
            <input type="hidden" value="<?=$agency_code ?>" id="agency_code" name="agency_code"  required>
          </div>
          <div class="pull-right" style="width:20%;text-align:right;">
              <i class="txtArrow ion ion-chevron-down"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="txtDate <?=$disabled." ".$disabled2 ?>" id="boxService"  onclick="showService()" >
          <div class="pull-left" style="width:80%">
            <span>งานบริการ</span>
            <div class="txtSelect" id="dateSelectService"><?= $service_name ?></div>
            <input type="hidden" value="<?= $service_id ?>" id="service_id" name="service_id" class="form-control" required>
          </div>
          <div class="pull-right" style="width:20%;text-align:right;">
              <i class="txtArrow ion ion-chevron-down"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <div class="txtDate <?=$disabled ?>" id="boxDateReserve" onclick="showReserveDate()" <?=$disabled ?>>
          <div class="pull-left" style="width:80%">
            <span>วันและช่วงเวลา</span>
            <div class="txtSelect" id="dateSelectDateReserve"><?= $dateReserve ?></div>
            <input type="hidden" value="<?= $date_reserve ?>" id="date_reserve" name="date_reserve" class="form-control" required>
            <input type="hidden" value="<?= $time_start ?>" id="time_start" name="time_start" class="form-control" required>
            <input type="hidden" value="<?= $time_end ?>" id="time_end" name="time_end" class="form-control" required>
          </div>
          <div class="pull-right" style="width:20%;text-align:right;">
              <i class="txtArrow ion ion-chevron-down"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-12">
       <label>รายการเอกสารที่ต้องใช้</label>
       <div class="<?=$disabled." ".$disabled2 ?>" style="height:200px;width:100%;border:1px solid #ccc;overflow:auto;text-align:justify;padding:10px;font-size: 18px" id="check_list">
         <?= $check_list; ?>
       </div>
    </div>
    <?php

    $disabled = "";
    $sql   = "SELECT * FROM t_consent WHERE consent_id = '1'";
    $query      = DbQuery($sql,null);
    $jsonc      = json_decode($query, true);
    $dataCountC = $jsonc['dataCount'];



    if($dataCountC > 0)
    {
        $disabled = "disabled";
        $consent_desc  = $jsonc['data'][0]['consent_desc'];
    ?>
    <div class="col-md-12">
      <div class="form-group" style="margin-top: 20px;margin-bottom: 0px;">
        <label>นโยบายความเป็นส่วนตัว</label>
        <div style="height:200px;width:100%;border:1px solid #ccc;overflow:auto;text-align:justify;padding:10px;font-size: 18px">
          <?= $consent_desc ?>
        </div>
      </div>
    </div>
    <?php if($action != "VIEW")
    {
    ?>
    <div class="col-md-12">
      <div class="checkbox">
        <div class="form-group" style="margin-bottom: 0px;">
        <label> <input type="checkbox" value="Y" name="accept_consent"  id="accept_consent"  onclick="checkAccept()"  required>ข้าพเจ้ายอมรับข้อกำหนดและเงื่อนไขการให้บริการนี้</label>
        <div/>
      </div>
    </div>
  </div>
<?php }} ?>
  </div>
</div>
<div class="box-footer" align="right">
  <?php if($action == "VIEW")
  {
  ?>
    <button type="button" class="btn btn-default btn-flat" style="width:100px;" onclick="gotoPage('../queue_list/');">กลับ</button>
  <?php
  }else{
  ?>
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" onclick="showForm();">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;" id="btnSubmit"  <?= $disabled ?>>บันทึก</button>
<?php } ?>
</div>
