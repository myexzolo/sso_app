<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$bg         = "background-color:#fff;";
?>
<div class="modal-body no-padding" style="overflow:auto;height:250px;">
    <ul class="nav nav-stacked">
    <?php
    $q = $_POST['q'];
    $con = "";
    if($q != "")
    {
      $con = " and agency_name like '%$q%'";
    }

    $sqls   = "SELECT *
               FROM t_agency
               where is_active = 'Y' and agency_code != '1000' $con
               ORDER BY agency_code";

    //echo $sqls;
    $querys     = DbQuery($sqls,null);
    $json       = json_decode($querys, true);
    $errorInfo  = $json['errorInfo'];
    $dataCount  = $json['dataCount'];
    $rows       = $json['data'];

    if($dataCount > 0)
    {
      foreach ($rows as $key => $value) {
        $is_active        = $value['is_active'];
        $agency_code        = $value['agency_code'];
        $agency_name      = ltrim(str_replace("สำนักงานประกันสังคม","",$value['agency_name']));
    ?>
    <li><a href="#" onclick="setAgencyCode('<?= $agency_code ?>','<?= $agency_name ?>')"><?= $agency_name ?></a></li>
    <?php
      }
    }
    ?>
    </ul>
</div>
<div class="modal-footer">
  <input type="hidden" id='agencyCode'>
  <input type="hidden" id='agencyName'>
  <button type="button" class="btn btn-flat bg-navy btn-block" onclick="setDataAgency()">ตกลง</button>
</div>
