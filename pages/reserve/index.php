<!DOCTYPE html>
<?php
$pageName     = "จองคิวล่วงหน้า";
$pageCode     = "reserve";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/reserve.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php
          $mem_id = @$_GET['mem_id'];
          if($mem_id != "" && !isset($_SESSION['member']['mem_name']))
          {
            $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
            //echo $sqls;
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true);
            $counts = $json['dataCount'];
            $rows   = $json['data'];
            if($counts == 1)
            {
              $_SESSION['member'] = $rows[0];
            }
          }

          $mem_name   = $_SESSION['member']['mem_name'];
          $mem_last   = $_SESSION['member']['mem_last'];

          include("../../inc/header.php");
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');

          $tr_id   = @$_POST['tr_id'];
          $action  = @$_POST['action'];

        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1><?= $pageName ?></h1>
            <ol class="breadcrumb">
              <li><a href="../home/"><i class="ion ion-android-home"></i> Home</a></li>
              <li class="active"><?= $pageCode ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <input type="hidden" value="<?= $tr_id ?>" id="trId">
              <input type="hidden" value="<?= $action ?>" id="action">
              <div class="reg-box">
                <!-- /.login-logo -->
                <div class="col-md-8 col-md-offset-2">
                  <div class="box box-solid" style="min-height:410px;">
                    <form id="formAED" novalidate enctype="multipart/form-data">
                    <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                      <div id="show-form"></div>
                    </form>
                  </div>
                  <!-- /.login-box-body -->
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModalAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">
                <div class="modal-content">
                  <div class="modal-header">
                    <div class="input-group">
                      <input type="text" id="q" class="form-control" placeholder="Search..." style="width: 300px;" onkeyup="searchAgency()">
                      <span class="input-group-btn">
                        <button type="button" name="search" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search" style="color:#b3b3b3"></i>
                        </button>
                      </span>
                    </div>
                  </div>
                  <div id="show-form-agency"></div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModalService" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">
                <div class="modal-content">
                  <div id="show-form-service"></div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModalDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-sm" role="document" style="margin-top: 25vh;">
                <div class="modal-content">
                  <div id="show-form-reserve"></div>
                </div>
              </div>
            </div>
              <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/reserve.js"></script>

      <script>
          $(".select2").select2();
          // showPerformanceInfo();
      </script>
    </body>
  </html>
