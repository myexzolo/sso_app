$(function ()
{
  showForm();
});

function showForm(){
  var trId = $("#trId").val();
  var action = $("#action").val();

  //console.log(trId + ","+ action);

  $.post("ajax/formAED.php",{action:action,tr_id:trId})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}
function searchAgency()
{
  var q = $("#q").val();
  if(q.length >= 5)
  {
    $.post("ajax/showAgency.php",{q:q})
      .done(function( data ) {
        $('#show-form-agency').html(data);
    });
  }else if (q.length == 0)
  {
    $.post("ajax/showAgency.php",{q:q})
      .done(function( data ) {
        $('#show-form-agency').html(data);
    });
  }
}

function checkAccept()
{
  var checked = $("#accept_consent").prop('checked');
  if(checked)
  {
    $("#btnSubmit").prop("disabled", false);
  }else
  {
    $("#btnSubmit").prop("disabled", true);
  }
}


function setAgencyCode(agency_code,agency_name)
{
    $("#agencyCode").val(agency_code);
    $("#agencyName").val(agency_name);
}

function setServiceId(serviceId,serviceName,checkList)
{
    $("#serviceId").val(serviceId);
    $("#serviceName").val(serviceName);
    $("#checkList").val(checkList);
}

function setDate(date,dateTH)
{
    $("#dateReserve").val(date);
    $("#dateThReserve").val(dateTH);
}

function setTime(fullTime,timeStart,timeEnd)
{
    $("#timeReserve").val(fullTime);
    $("#timeStart").val(timeStart);
    $("#timeEnd").val(timeEnd);
}

function setDataAgency()
{
  if($("#agencyCode").val() != ""){
    $("#dateSelectAgency").html($("#agencyName").val());
    $("#agency_code").val($("#agencyCode").val());
    $('#boxAgency').removeClass("validate");

  }else{
    $("#dateSelectAgency").html("-- กรุณาเลือก --");
    $("#agency_code").val("");
    $("#check_list").html("");
  }

  $('#myModalAgency').modal('toggle');
}

function setDataDate()
{
  if($("#dateReserve").val() != "")
  {
    $("#date_reserve").val($("#dateReserve").val());
    $.post("ajax/showTime.php",{dateReserve:$("#dateThReserve").val(),dateR:$("#date_reserve").val()})
      .done(function( data ) {
        $('#show-form-reserve').html(data);
    });
  }else{
    $("#dateSelectDateReserve").html("-- กรุณาเลือก --");
    $("#date_reserve").val("");
    $("#time_start").val("");
    $("#time_end").val("");
  }
}

function setDatatime()
{
  if($("#serviceId").val() != ""){
    var dateReserve  = $("#dateReserve").val() + " " + $("#timeReserve").val();
    $("#dateSelectDateReserve").html(dateReserve);
    $("#time_start").val($("#timeStart").val());
    $("#time_end").val($("#timeEnd").val());

    $('#boxDateReserve').removeClass("validate");
  }else{
    $("#dateSelectDateReserve").html("-- กรุณาเลือก --");
    $("#date_reserve").val("");
    $("#time_start").val("");
    $("#time_end").val("");
  }

  $('#myModalDate').modal('toggle');
}


function setDatatime2()
{
  var timeStart = $("#ts1").val() + ":" + $("#ts2").val();
  var timeEnd   = $("#te1").val() + ":" + $("#te2").val();
  var fullTime  = timeStart + " - " + timeEnd;

  $("#timeReserve").val(fullTime);
  $("#timeStart").val(timeStart);
  $("#timeEnd").val(timeEnd);

  if($("#serviceId").val() != ""){
    var dateReserve  = $("#dateReserve").val() + " " + $("#timeReserve").val();
    $("#dateSelectDateReserve").html(dateReserve);
    $("#time_start").val(timeStart);
    $("#time_end").val(timeEnd);

    $('#boxDateReserve').removeClass("validate");
  }else{
    $("#dateSelectDateReserve").html("-- กรุณาเลือก --");
    $("#date_reserve").val("");
    $("#time_start").val("");
    $("#time_end").val("");
  }
  $('#myModalDate').modal('toggle');
}

function setDataService()
{
  if($("#serviceId").val() != ""){
    $("#dateSelectService").html($("#serviceName").val());
    $("#service_id").val($("#serviceId").val());
    $("#check_list").html(decodeURIComponent($("#checkList").val()));
    $('#boxService').removeClass("validate");
  }else{
    $("#dateSelectService").html("-- กรุณาเลือก --");
    $("#service_id").val("");
    $("#check_list").html("");
  }

  $('#myModalService').modal('toggle');
}

function showAgency(q)
{
  $.post("ajax/showAgency.php",{q:q})
    .done(function( data ) {
      $('#myModalAgency').modal('toggle');
      $('#show-form-agency').html(data);
  });
}

function showReserveDate()
{
  $.get("ajax/showDate.php")
    .done(function( data ) {
      $('#myModalDate').modal('toggle');
      $('#show-form-reserve').html(data);
  });
}

function showService()
{
  var agency_code = $("#agency_code").val();
  if(agency_code != "")
  {
    $.post("ajax/showService.php",{agency_code:agency_code})
      .done(function( data ) {
        $('#myModalService').modal('toggle');
        $('#show-form-service').html(data);
    });
  }
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if($('#agency_code').val() == "")
  {
    $('#boxAgency').addClass("validate");
  }

  if($('#service_id').val() == "")
  {
    $('#boxService').addClass("validate");
  }

  if($('#time_end').val() == "")
  {
    $('#boxDateReserve').addClass("validate");
  }


  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        //showTable();
        gotoPage('../queue_list/');
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
