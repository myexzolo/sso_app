<!DOCTYPE html>
<?php
$pageName     = "Dashboard";
$pageCode     = "Home";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list  = $_SESSION['member'][0]['role_list'];
          $user_login = $_SESSION['member'][0]['user_login'];

          $agencyCode = $_SESSION['AGENCY_CODE'];

          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $venDorID = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

          $RoleAdmin = false;
          $roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
          // print_r($_SESSION['member']);
          if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)|| in_array("PFIT", $roleCodeArr)){
            $RoleAdmin = true;
          }
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>Dashboard
                <button type="button" id="btn1_bg"  class="btn btn-flat bg-navy" style="width:60px;"  onclick="searchQueue('1')">วันนี้</button>
                <button type="button" id="btn15_bg" class="btn btn-flat bg-teal" style="width:60px;" onclick="searchQueue('15')">15 วัน</button>
                <button type="button" id="btn30_bg" class="btn btn-flat bg-teal" style="width:60px;" onclick="searchQueue('30')"> 30 วัน</button>
            </h1>
            <ol class="breadcrumb">
              <li><a href=""><i class="fa fa-dashboard"></i> Dashboard</a></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-lg-3 col-xs-6">
          <!-- small box -->
                <div class="small-box bg-purple">
                  <div class="inner">
                    <h3>250</h3>
                    <p style="font-size: 18px;">ผู้มารับบริการ</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-wheelchair"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-purple-active">
                    <p class="fagency">งานสิทธิประโยชน์ทดแทน</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
          <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>1,050</h3>
                    <p style="font-size: 18px;">ผู้มารับบริการ</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-bitcoin"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-aqua-active">
                    <p class="fagency">งานการเงินและบัญชี</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
          <!-- small box -->
                <div class="small-box bg-light-blue">
                  <div class="inner">
                    <h3>50</h3>
                    <p style="font-size: 18px;">ผู้มารับบริการ</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-suitcase"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-light-blue-active">
                    <p class="fagency">งานเงินสมทบ</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
          <!-- small box -->
                <div class="small-box bg-blue">
                  <div class="inner">
                    <h3>500</h3>
                    <p style="font-size: 18px;">ผู้มารับบริการ</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-person"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-blue-active">
                    <p class="fagency">งานทะเบียน</p>
                  </a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div id="show-data"></div>
              </div>
              <div class="col-md-6">
                <div id="show-performance"></div>
              </div>
            </div>
              <!-- /.row -->
            </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script src="js/chart.js"></script>

      <script>
          $(".select2").select2();
          showKiosk(<?=$agencyCode;?>);
          // showPerformanceInfo();
      </script>
    </body>
  </html>
