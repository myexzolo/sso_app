<style>
.knob-label
{
  font-size: 20px !important;
}
.btn-info
{
  font-size: 20px !important;
  line-height: 20px;
  color: #ffffff;
}
<?php
$agencyCode = $_POST['agencyCode'];
$kioskCode  = $_POST['kioskCode'];
?>
</style>
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Kiosk</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12 text-center">
        <label><b>Kiosk2</b></label>
      </div>
      <div class="col-md-4 text-center">
        <input type="text" class="knob" value="90" data-width="90" data-height="90" data-fgColor="#932ab6">
        <div class="knob-label">CPU</div>
      </div>
      <!-- ./col -->
      <div class="col-md-4 text-center">
        <input type="text" class="knob" value="50" data-width="90" data-height="90" data-fgColor="#39CCCC">
        <div class="knob-label">Memory</div>
      </div>

      <div class="col-md-4 text-center">
        <input type="text" class="knob" value="50" data-width="90" data-height="90" data-fgColor="#00c0ef">
        <div class="knob-label">Disk</div>
      </div>
      <div class="col-md-12">
        <button type="button" class="btn btn-block btn-info btn-flat" onclick="showKiosk(<?=$agencyCode;?>);">
          กลับ
        </button>
      </div>
      <!-- ./col -->
    </div>
  </div>
  <!-- /.box-body -->
</div>
<script src="../../plugins/jquery-knob/jquery.knob.min.js"></script>
<script>
  $(function () {
    /* jQueryKnob */

    $('.knob').knob({
      /*change : function (value) {
       //console.log("change : " + value);
       },
       release : function (value) {
       console.log("release : " + value);
       },
       cancel : function () {
       console.log("cancel : " + this.value);
       },*/
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a   = this.angle(this.cv)  // Angle
            ,
              sa  = this.startAngle          // Previous start angle
            ,
              sat = this.startAngle         // Start angle
            ,
              ea                            // Previous end angle
            ,
              eat = sat + a                 // End angle
            ,
              r   = true

          this.g.lineWidth = this.lineWidth

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3)

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value)
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3)
            this.g.beginPath()
            this.g.strokeStyle = this.previousColor
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false)
            this.g.stroke()
          }

          this.g.beginPath()
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false)
          this.g.stroke()

          this.g.lineWidth = 2
          this.g.beginPath()
          this.g.strokeStyle = this.o.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false)
          this.g.stroke()

          return false
        }
      }
    })
  })

</script>
