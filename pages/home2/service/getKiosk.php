<style>
.knob-label
{
  font-size: 20px !important;
}
.kiosk
{
  font-size: 80px;
}

.closed
{
  color: #cccccc;
}

.error
{
  color: #ffffff !important;
}

.open
{
  cursor: pointer;
  color: #adadeb !important;
}
.open :hover
{
  color: #8484e1 !important;
}

.kos
{
  margin-bottom: 20px;
}

<?php
$agencyCode = $_POST['agencyCode'];
?>

</style>
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Kiosk</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6 text-center kos">
        <button type="button" class="btn btn-block btn-flat disabled" style="height:200px">
          <i class="icon ion-android-desktop kiosk closed"></i>
          <div class="knob-label"><b>Kiosk1 (ปิดบริการ)</b><br>เปิดเครื่อง 07:00 - 16:30 น.</div>
        </button>
      </div>
      <div class="col-md-6 text-center kos">
        <button type="button" class="btn btn-block btn-danger btn-flat disabled" style="height:200px">
          <i class="icon ion-android-desktop kiosk error"></i>
          <div class="knob-label error">Kiosk1 (ระบบมีปัญหา)<br>เปิดเครื่อง 07:00 - 16:30 น.</div>
        </button>
      </div>
      <div class="col-md-6 text-center kos">
        <button type="button" class="btn btn-block btn-flat" style="height:200px" onclick="showPerformanceInfo('1001','Kiosk01')">
          <i class="icon ion-android-desktop kiosk open"></i>
          <div class="knob-label"><b>Kiosk2 (เปิดบริการ)</b><br>เปิดเครื่อง 07:00 - 16:30 น.</div>
        </button>
      </div>
      <!-- ./col -->
    </div>
  </div>
  <!-- /.box-body -->
</div>
