<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">จำนวนผู้มารับบริการ แบ่งตามช่วงเวลา</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="chart">
      <canvas id="barChart" style="height: 230px; width: 554px;" height="230" width="554"></canvas>
    </div>
  </div>
  <!-- /.box-body -->
</div>

<script>
function initBarChart()
{
  //-------------
    //- BAR CHART -
    //-------------

    var ctx = document.getElementById('barChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['07:00 - 09:00', '09:01 - 11:00', '11:01 - 13:00', '13:01 - 15:00', '15:01 - 17:00'],
            datasets: [{
                label: 'สิทธิประโยชน์ทดแทน',
                data: [12, 19, 40, 20, 2],
                backgroundColor: 'rgba(255, 99, 132, 1)',
            },{
                label: 'งานการเงินและบัญชี',
                data: [30, 49, 33, 55, 10],
                backgroundColor: 'rgba(54, 162, 235, 1)'
            },{
                label: 'งานเงินสมทบ',
                data: [5, 9, 13, 6, 2],
                backgroundColor: 'rgba(255, 206, 86, 1)'
            },{
                label: 'งานทะเบียน',
                data: [1, 5, 3, 10, 2],
                backgroundColor: 'rgba(75, 192, 192, 1)'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        fontFamily : 'CSChatThai',
                        fontSize : 16
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontFamily : 'CSChatThai',
                        fontSize : 16
                    }
                }],
            },
            legend: {
              labels: {
                fontFamily: 'CSChatThai',
                fontSize : 18
              }
            }
        }
    });
}
</script>
