<!DOCTYPE html>
<?php
$pageName     = "แสดงความคิดเห็น";
$pageCode     = "question";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/queue_quest.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php
        
          $mem_id = @$_GET['mem_id'];
          if($mem_id != "" && !isset($_SESSION['member']['mem_name']))
          {
            $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
            //echo $sqls;
            $querys = DbQuery($sqls,null);
            $json   = json_decode($querys, true);
            $counts = $json['dataCount'];
            $rows   = $json['data'];
            if($counts == 1)
            {
              $_SESSION['member'] = $rows[0];
            }
          }

          $mem_name   = $_SESSION['member']['mem_name'];
          $mem_last   = $_SESSION['member']['mem_last'];

          include("../../inc/header.php");
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../home/"><i class="ion ion-android-home"></i> Home</a></li>
              <li class="active"><?= $pageCode ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <div class="row">
              <div class="col-md-4">

                <table style="width:100%;border: 1px solid #ece6e6">
                  <tr>
                    <td class="tableReserve">
                      <button type="button" id="btnR" class="btn btn-block btn-flat btnReserve" onclick="showList('R');" >รอแสดงคิดเห็น</button>
                    </td>
                    <td class="tableReserve">
                        <button type="button" id="btnS" class="btn btn-block btn-flat btnReserve tRBl" onclick="showList('S');">แสดงคิดเห็นแล้ว</button>
                    </td>
                    <td class="tableReserve" style="display:none">
                        <button type="button" id="btnA" class="btn btn-block btn-flat btnReserve tRBl" onclick=""><i class="fa fa-qrcode"></i>&nbsp;&nbsp;เพิ่ม</button>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="col-md-12">
                <div style="padding: 5px;background-color: #eaeaead1;min-height:400px;border: 1px solid #ece6e6">
                  <div class="row" id="queue_list"></div>
                </div>
              </div>
            </div>
            <!-- Main row -->
              <!-- /.row -->
          </section>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document" >

              <div class="modal-content">
                <div class="modal-header">
                    <h3 class="box-title" style="margin-top: 5px; margin-bottom: 5px;">
                      <i class="fa fa-commenting" style="color:green;"></i>&nbsp;แสดงความคิดเห็น
                    </h3>
                </div>
                <form id="formAED" novalidate enctype="multipart/form-data">
                <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form"></div>
                </form>
              </div>
            </div>
          </div>

          <div class="modal fade" id="myModalQR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document" >
              <div class="modal-content">
                <div class="modal-header">
                    <h3 class="box-title" style="margin-top: 5px; margin-bottom: 5px;">
                      <i class="fa fa-qrcode" style="color:green;"></i>&nbsp;สแกน QR
                    </h3>
                </div>
                <div class="modal-body">

                </div>
              </div>
            </div>
          </div>


          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/queue_quest.js"></script>

      <script>
          $(".select2").select2();
          $("#btnReservR").focus();
      // showPerformanceInfo();
      </script>
    </body>
  </html>
