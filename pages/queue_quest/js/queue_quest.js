$(function ()
{
  $("#btnR").addClass('btnReserveFocus');
  showList("R");
  // openModalQR();
});

function showList(status){
  if(status == "R"){
    $("#btnR").addClass('btnReserveFocus');
    $("#btnS").removeClass('btnReserveFocus');
    $("#btnA").removeClass('btnReserveFocus');
  }else if(status == "S")
  {
    $("#btnR").removeClass('btnReserveFocus');
    $("#btnS").addClass('btnReserveFocus');
    $("#btnA").removeClass('btnReserveFocus');
  }

  $.post("ajax/list.php",{status:status})
    .done(function( data ) {
      $('#queue_list').html(data);
  });
}


function openModalQR()
{
  $('#myModalQR').modal({backdrop:'static'});
}


function openModalQuest(trans_queue_id,status)
{
  $.post("ajax/form.php",{trans_queue_id:trans_queue_id,status:status})
    .done(function( data ) {
      $('#show-form').html(data);
      $('#myModal').modal({backdrop:'static'});

  });
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        showList("R")
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
