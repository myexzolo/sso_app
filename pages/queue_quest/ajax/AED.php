<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');


$trans_queue_id     = isset($_POST['trans_queue_id'])?$_POST['trans_queue_id']:"";
$question_id        = isset($_POST['question_id'])?$_POST['question_id']:"";
$question_code      = isset($_POST['question_code'])?$_POST['question_code']:"";
$question_type      = isset($_POST['question_type'])?$_POST['question_type']:"";
$result_score       = isset($_POST['result_score'])?$_POST['result_score']:"";
$result_text        = isset($_POST['result_text'])?$_POST['result_text']:"";

$sql = "";
// print_r($_POST);

$num = count($question_id);
for($x=0; $x < $num; $x++)
{
  $questionId       = $question_id[$x];
  $questionCode     = $question_code[$x];
  $questionType     = $question_type[$x];
  $resultScore      = $result_score[$questionId];
  $resultText       = $result_text[$x];

  $arr['trans_queue_id']  = $trans_queue_id;
  $arr['question_code']   = $questionCode;
  if($questionType == "S"){
    $arr['result_score']    = $resultScore;
  }else{
    $arr['result_text']     = $resultText;
  }

  $sql .= DBInsertPOST($arr,'t_questionnaire_result');
}

if($sql != "")
{
  $arrt['trans_queue_id']  = $trans_queue_id;
  $arrt['is_result']  = 'Y';
  $sql .= DBUpdatePOST($arrt,'t_trans_queue','trans_queue_id');
}

//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0)
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
