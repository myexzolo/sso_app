<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$mem_id     = $_SESSION["member"]['mem_id'];
$mem_name   = $_SESSION["member"]['mem_name'];
$mem_last   = $_SESSION["member"]['mem_last'];
$tel_no     = $_SESSION["member"]['tel_no'];
$email      = $_SESSION["member"]['email'];
$hide_list  = $_SESSION["member"]['hide_list'];

$datenow = date('Y-m-d');

$con = "";
$status = $_POST['status'];

if($status == "R")
{
  $con = " and t.is_result = 'N'";

  $strBtn = "แสดงความคิดเห็น";
  $icon   = "fa fa-clipboard";

}else if($status == "S")
{
  $con =  " and t.is_result = 'Y'";
  $strBtn = "ดูแสดงความคิดเห็น";
  $icon   = "fa fa-search";
}

$sqls   = "SELECT t.*, a.agency_name, s.service_name_a, u.user_name
           FROM t_trans_queue t
           LEFT JOIN t_agency a ON t.agency_code = a.agency_code
           LEFT JOIN t_service_agency s ON t.service_id = s.service_id and t.agency_code = s.agency_code
           LEFT JOIN t_user u ON t.user_ref_code = u.ref_code
           where  t.member_id = '$mem_id' and status_queue = 'E' $con
           ORDER BY t.date_end DESC";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

if($dataCount > 0)
{
  foreach ($rows as $key => $value) {
    $agency_name = ltrim(str_replace("สำนักงานประกันสังคม","",$value['agency_name']));

    $dateStartService = dateTimetoThai($value['date_start']);

    $arrData['agency_name'] = $agency_name;
    $arrData['ref_queue']   = $value['ref_queue'];
    $arrData['queue_code']  = $value['queue_code'];
    $arrData['dateStartService'] = $dateStartService;
    $arrData['service_name_a'] = $value['service_name_a'];
    $arrData['trans_queue_id'] = $value['trans_queue_id'];
    $jsonData = json_encode($arrData);



?>

<div class="col-md-4">
  <div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-commenting"></i></span>
    <div class="info-box-content">
      <div>
        <span class="info-box-text">สำนักงานประกันสังคม</span>
        <span class="info-box-text2"><?= $agency_name;?></span>
      </div>
    </div>
      <table style="margin-left:10px;">
        <tr>
          <td style="width: 96px;">รหัสคิว</td>
          <td style="width: 10px;text-align:center;">:</td>
          <td><div class="info-box-text3"><?= $value['queue_code'] ?></div>
          </td>
        </tr>
       <tr>
         <td style="width: 90px;">งานบริการ</td>
         <td style="width: 10px;text-align:center;">:</td>
         <td><div class="info-box-text3"><?= $value['service_name_a'] ?></div></td>
       </tr>
       <tr>
         <td>วันที่รับบริการ</td>
         <td style="text-align:center;">:</td>
         <td><div class="info-box-text3"><?= $dateStartService ?></div></td>
       </tr>
       <tr>
         <td>เจ้าหน้าที่บริการ</td>
         <td style="text-align:center;">:</td>
         <td><div class="info-box-text3"><?= $value['user_name'] ?></div></td>
       </tr>
       </table>
      <div>
        <table style="margin-left:10px;margin-right:10px;" class="btn-q">
        <tr>
          <td colspan="3" style="border-top: 1px solid #ece6e6;padding-top:4px">
            <button type="button" class="btn btn-block btn-flat btn-default" onclick="openModalQuest('<?= $value['trans_queue_id'] ?>','<?= $status ?>')">
              <i class="<?=$icon?>"></i> <?= $strBtn ?>
            </button>
          </td>
        </tr>
      </table>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>

<?php } } ?>
