<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action             = $_POST['status'];
$trans_queue_id     = isset($_POST['trans_queue_id'])?$_POST['trans_queue_id']:"";


$disabled   = "";
$bg         = "background-color:#fff;";
$display    = "";


if($action == 'S')
{
  $disabled = "disabled";
  $bg       = "";
  $display    = "display:none;";
}

  $sql   = "SELECT q.*, qr.result_score, qr.result_text
           FROM b_questionnaire q
           LEFT JOIN t_questionnaire_result qr ON q.question_code = qr.question_code and trans_queue_id = '$trans_queue_id'
           WHERE q.is_active = 'Y' order by q.question_code";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  //echo $sql;


?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="trans_queue_id" value="<?=$trans_queue_id?>">
<div class="modal-body">
    <div class="row">
      <?php
          if($dataCount > 0)
          {
            foreach ($rows as $key => $value)
            {
              $question_id      = $value['question_id'];
              $question_code    = $value['question_code'];
              $question_detail  = $value['question_detail'];
              $question_type    = $value['question_type'];
              $max_score        = $value['max_score'];
              $result_score     = @$value['result_score'];
              $result_text      = @$value['result_text'];

      ?>
      <div class="col-md-12">
        <input type="hidden"  value="<?= $question_id ?>" name="question_id[]">
        <input type="hidden"  value="<?= $question_code ?>" name="question_code[]">
        <input type="hidden"  value="<?= $question_type ?>" name="question_type[]">
        <div class="form-group" style="margin-bottom: 0px;">
          <label><?= $question_detail ?></label>
        </div>
          <?php
            if($question_type == "S")
            {
              $sqla   = "SELECT * FROM b_answer WHERE question_id = '$question_id' order by score DESC";

              $query      = DbQuery($sqla,null);
              $jsona      = json_decode($query, true);
              $dataCounta = $jsona['dataCount'];
              $rowa       = $jsona['data'];
              if($dataCounta > 0)
              {
                echo "<div class=\"form-group\">";
                foreach ($rowa as $valuea)
                {
                  $score        = $valuea['score'];
                  $answer_desc  = $valuea['answer_desc'];

                  $checked = ($score == $result_score ?"checked":"");

                  echo"<div class=\"radio\">
                        <label>
                          <input type=\"radio\" name=\"result_score[$question_id]\" $checked $disabled value=\"$score\"  required>$answer_desc</label>
                      </div>";
                }
                echo "</div>";
              }else{
                echo "<input type=\"hidden\"  value=\"\" name=\"result_score[$question_id]\">";
              }
              echo "<input type=\"hidden\"  value=\"\" name=\"result_text[]\">";
            }else{
              echo "<div class=\"form-group\">
                      <input type=\"hidden\"  value=\"\" name=\"result_score[]\">
                      <textarea name=\"result_text[]\" class=\"form-control\" rows=\"2\" $disabled placeholder=\"\">$result_text</textarea>
                    </div>";
            }
          ?>
      </div>
      <?php
            }
          }
      ?>
    </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:200px;<?=$display?>">แสดงความคิดเห็น</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
