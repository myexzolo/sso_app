<!DOCTYPE html>
<?php
$pageName     = "จองคิว";
$pageCode     = "Home";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบสนับสุนนการให้บริการ (Queue) - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php
         $mem_id = @$_GET['mem_id'];
         if($mem_id != "")
         {
           $sqls   = "SELECT * FROM t_member WHERE 	mem_id = '$mem_id' and is_active = 'Y' ";
           //echo $sqls;
           $querys = DbQuery($sqls,null);
           $json   = json_decode($querys, true);
           $counts = $json['dataCount'];
           $rows   = $json['data'];
           if($counts == 1)
           {
             $_SESSION['member'] = $rows[0];
           }
         }
         include("../../inc/header.php");
         ?>

        <?php
          include("../../inc/sidebarMember.php");
          include('../../inc/function/mainFunc.php');
          $mem_name   = $_SESSION['member']['mem_name'];
          $mem_last   = $_SESSION['member']['mem_last'];

          $consent_data_id	= $_SESSION['member']['consent_data_id'];

        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>หน้าหลัก</h1>
            <ol class="breadcrumb">
              <li><a href=""><i class="ion ion-android-home"></i>home</a></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-lg-3 col-xs-6" onclick="gotoPage('../reserve/')">
          <!-- small box -->
                <div class="small-box bg-purple">
                  <div class="icon-btn">
                    <i class="ion ion-android-calendar"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-purple-active">
                    <p class="fagency">จองคิว</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6" onclick="gotoPage('../queue_list/')">
          <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="icon-btn">
                    <i class="ion ion-android-list"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-aqua-active">
                    <p class="fagency">ประวัติการจองคิว</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6" onclick="gotoPage('../queue_check/')">
          <!-- small box -->
                <div class="small-box bg-light-blue">
                  <div class="icon-btn">
                    <i class="ion ion-search"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-light-blue-active">
                    <p class="fagency">ตรวจสอบคิว</p>
                  </a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6" onclick="gotoPage('../queue_quest/')">
          <!-- small box -->
                <div class="small-box bg-blue">
                  <div class="icon-btn">
                    <i class="ion ion-chatbox-working"></i>
                  </div>
                  <a href="#" class="small-box-footer bg-blue-active">
                    <p class="fagency">แสดงความคิดเห็น</p>
                  </a>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                  <div id="show-member"></div>
              </div>
            </div>

            <div class="modal fade" id="myModalConsent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-md" role="document" style="margin-top: 25vh;">
                <div class="modal-content">
                  <form id="formAED" novalidate enctype="multipart/form-data">
                  <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form-consent"></div>
                  </form>
                </div>
              </div>
            </div>
              <!-- /.row -->
            </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>

      <script>
          $(".select2").select2();
          // showPerformanceInfo();
      </script>
    </body>
  </html>
