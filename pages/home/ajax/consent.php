<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$mem_id    = isset($_POST['id'])?$_POST['id']:"";

$sql   = "SELECT * FROM t_consent WHERE consent_id = '1'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'][0];

$bg         = "background-color:#fff;";

?>
<div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <input type="hidden" name="mem_id" value="<?=$mem_id?>">
        <div class="form-group" style="margin-bottom: 0px;">
          <label>นโยบายความเป็นส่วนตัว</label>
          <div style="height:400px;width:100%;border:1px solid #ccc;overflow:auto;text-align:justify;padding:10px;font-size: 18px">
            <?= $row['consent_desc'] ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="checkbox">
          <div class="form-group" style="margin-bottom: 0px;">
          <label> <input type="checkbox" value="Y" name="accept_consent"  id="accept_consent"  onclick="checkAccept()"  required>ข้าพเจ้ายอมรับข้อกำหนดและเงื่อนไขการให้บริการนี้</label>
          <div/>
        <?php
          $sqls       = "SELECT * FROM t_member where mem_id = '$mem_id'";
          $query      = DbQuery($sqls,null);
          $json       = json_decode($query, true);
          $rowm       = $json['data'][0];

          $consent_data_id = $rowm['consent_data_id'];


          $sql2   = "SELECT * FROM t_consent_data WHERE consent_id = '1'";

          $query      = DbQuery($sql2,null);
          $json       = json_decode($query, true);
          $dataCount  = $json['dataCount'];
          $row2       = $json['data'];
          if($dataCount > 0)
          {
            foreach ($row2 as $key => $value)
            {
                $checked = "";
                if($value['consent_data_id'] == $consent_data_id)
                {
                  $checked = "checked";
                }
        ?>
        <div class="radio">
          <label> <input value="<?= $value['consent_data_id']."|".$value['hide_list'] ?>" <?=$checked?> name="hide_list" type="radio"><?= $value['consent_data_name'] ?></label>
        </div>
        <?php
            }
          }
        ?>
      </div>
    </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;" id="btnSubmit" disabled>บันทึก</button>
</div>
