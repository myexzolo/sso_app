<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$mem_name   = $_SESSION['member']['mem_name'];
$mem_last   = $_SESSION['member']['mem_last'];

$consent_data_id	= $_SESSION['member']['consent_data_id'];

$sql   = "SELECT * FROM t_consent_data where consent_data_id = '$consent_data_id'";

$query     = DbQuery($sql,null);
$json      = json_decode($query, true);
$row       = $json['data'][0];

$consent_data_name  = $row['consent_data_name'];

?>
<div class="box box-widget widget-user-2">
<!-- Add the bg color to the header using any of the bg-* classes -->
<div class="widget-user-header bg-yellow">
  <div class="widget-user-image" >
    <img style="border: 3px solid #fff;" src="../../image/user3.png" onerror="this.onerror='';this.src='../../image/user3.png'" class="img-circle" alt="User Image">
  </div>
  <!-- /.widget-user-image -->
  <h3 class="widget-user-username"><?= $mem_name." ".$mem_last ?>
      <span class="pull-right"><i class="fa fa-pencil" style="font-size:20px;"></i></span>
  </h3>
  <h5 class="widget-user-desc">ผู้ใช้บริการ</h5>
</div>
<div class="box-footer no-padding">
  <ul class="nav nav-stacked">
    <li class="aa">วันเดือนปีเกิด <span class="pull-right"><?= datetoThaiFull($_SESSION['member']['mem_dob']) ?></span></li>
    <li class="aa">เบอร์โทร <span class="pull-right"><?= $_SESSION['member']['tel_no'] ?></span></li>
    <li class="aa">Email <span class="pull-right"><?= $_SESSION['member']['email'] ?></span></li>
    <li class="aa">เลขที่บัตรประชาชน <span class="pull-right"><?= $_SESSION['member']['mem_idcard'] ?></span></li>
    <li class="aa">ข้อมูลความเป็นส่วนตัว
      <i class="fa fa-pencil" onclick="showForm('EDIT','<?= $_SESSION['member']['mem_id'] ?>')" style="font-size:18px;color:#999;"></i>
      <span class="pull-right"><?=$consent_data_name?></span>
    </li>
  </ul>
</div>
</div>
