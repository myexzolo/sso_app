$(function () {
  showMember();
});


function showMember(){
  $.get("ajax/mem_info.php")
    .done(function( data ) {
      $('#show-member').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/consent.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModalConsent').modal({backdrop:'static'});
      $('#show-form-consent').html(data);
  });
}

function checkAccept()
{
  var checked = $("#accept_consent").prop('checked');
  if(checked)
  {
    $("#btnSubmit").prop("disabled", false);
  }else
  {
    $("#btnSubmit").prop("disabled", true);
  }
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  // $('.icons').remove('span');
  if ($('#formAED').smkValidate()) {
      $.ajax({
          url: 'ajax/AED.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
          $('#formAED').smkClear();
          $.smkAlert({text: data.message,type: data.status});
          showMember();
          $('#myModalConsent').modal('toggle');
        // }, 1000);
      }).fail(function (jqXHR, textStatus) {
          console.log(jqXHR);
      });
  }
});
