<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>


    <img class="image-logo image-logo-main" src="../../image/logo.png">
    <p class="res-queue">ระบบจองคิวรับบริการล่วงหน้า</p>

    <div class="box-home">
      <div class="box" onclick="goback('../registerqueue')">
        <img src="../../image/icon/booking.png">
        <p>จองคิว</p>
      </div>
      <div class="box" onclick="goback('../history')">
        <img src="../../image/icon/history.png">
        <p>ประวัติการจอง</p>
      </div>

    </div>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>
