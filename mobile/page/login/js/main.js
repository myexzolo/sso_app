$('#formLogin').on('submit', function(event) {
  event.preventDefault();

  if ($('#formLogin').smkValidate()) {
    // Code here
    $.ajax({
        url: 'service/getLogin.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type: data.status});
      if(data.status == 'success'){
        window.location='../home/';
      }
    });

  }

});
