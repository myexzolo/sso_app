<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>

    <form id="formLogin" novalidate>
      <div class="box-form">
        <div class="row">
          <div class="col-xs-12">
            <img class="image-logo" src="../../image/logo.png">
            <p class="login-text">ระบบจองคิวรับบริการล่วงหน้า</p>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <input type="text" name="mem_idcard" class="form-control" data-smk-msg="&nbsp;" placeholder="เลขที่บัตรประชาชน" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <input type="password" name="mem_pass" class="form-control" data-smk-msg="&nbsp;" placeholder="รหัสผ่าน" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="text-center">
              <button type="submit" class="btn btn-warning btn-sso">เข้าสู่ระบบ</button>
              <div class="text-center">
                <a href="../register" class="a-sso">สมัครสมาชิก</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>
