<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <?php $back='../home';$backname='กลับหน้าหลัก'; ?>
    <?php include "../../inc/back.php"; ?>
    <div class="box-form">

      <div class="row">
        <div class="col-xs-12">
          <img class="image-logo" src="../../image/logo.png">
        </div>
      </div>

      <form id="formRegister" novalidate>
      <!-- <form method="post" action="service/AEDregister.php"> -->
        <div class="row">
        <div class="col-xs-12">
          <div class="form-group">
            <select class="form-control" name="agency_code" required data-smk-msg="&nbsp;">
              <option value="">เลือกหน่วยงาน</option>
              <?php
                $sql = "SELECT * FROM t_agency WHERE is_active = 'Y'";
                $query = DbQuery($sql,null);
                $json = json_decode($query,true);
                if($json['dataCount'] > 0){
                  foreach ($json['data'] as $value) {
              ?>
              <option value="<?=$value['agency_code']?>"><?=$value['agency_name']?></option>
              <?php }} ?>
            </select>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="form-group">
            <select class="form-control" name="service_id" required data-smk-msg="&nbsp;">
              <option value="">เลือกงานบริการ</option>
              <?php
                $sql = "SELECT * FROM t_service WHERE is_active = 'Y' AND reserve_status = 'Y'";
                $query = DbQuery($sql,null);
                $json = json_decode($query,true);
                if($json['dataCount'] > 0){
                  foreach ($json['data'] as $value) {
              ?>
              <option value="<?=$value['service_id']?>"><?=$value['service_name']?></option>
              <?php }} ?>
            </select>
          </div>
        </div>

        <div class="col-xs-12">
          <label>วันที่จองคิว</label>
        </div>
        <div class="col-xs-4">
          <div class="form-group">
            <select class="form-control" name="day" required data-smk-msg="&nbsp;">
              <option value="">วันที่</option>
              <?php for ($i=1; $i <=31 ; $i++) { ?>
              <option value="<?=$i?>"><?=$i?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-4 pdd-0">
          <div class="form-group">
            <select class="form-control" name="month" required data-smk-msg="&nbsp;">
              <option value="">เดือน</option>
              <?php for ($i=1; $i <=12 ; $i++) { ?>
              <option value="<?=$i?>"><?=getMonth($i)?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-4">
          <div class="form-group">
            <select class="form-control" name="year" required data-smk-msg="&nbsp;">
              <option value="">ปี</option>
              <?php for ($i=date('Y')+543; $i <= date('Y')+543+1 ; $i++) { ?>
              <option value="<?=$i?>"><?=$i?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-12">
          <label>เวลา</label>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <select class="form-control" name="hour" required data-smk-msg="&nbsp;">
              <option value="">ชั่วโมง</option>
              <?php for ($i=8; $i <=16 ; $i++) { ?>
              <option value="<?=$i?>"><?=$i?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="form-group">
            <select class="form-control" name="minute" required data-smk-msg="&nbsp;">
              <option value="">นาที</option>
              <?php for ($i=0; $i <=55 ; $i=$i+15) { ?>
              <option value="<?=$i?>"><?=$i?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="text-center">
            <button type="submit" class="btn btn-warning btn-sso">จองคิว</button>
          </div>
        </div>
      </div>
      </form>
    </div>



    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>
