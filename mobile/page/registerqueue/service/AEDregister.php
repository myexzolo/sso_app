<?php

$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR;
$new_path = str_replace("page/registerqueue/service/","",str_replace("\\","/",$PNG_TEMP_DIR)).'image/qr/';

include "../../../../lib/qrcode/qrlib.php";
include '../../../../inc/function/connect.php';
include '../../../../inc/function/mainFunc.php';
include '../../../inc/function/mainFunc.php';

$_POST['mem_id'] = $_SESSION['member']['mem_id'];

$day = strlen($_POST['day'])==1?"0{$_POST['day']}":$_POST['day'];
$month = strlen($_POST['month'])==1?"0{$_POST['month']}":$_POST['month'];
$year = $_POST['year']-543;
$hour = strlen($_POST['hour'])==1?"0{$_POST['hour']}":$_POST['hour'];
$minute = strlen($_POST['minute'])==1?"0{$_POST['minute']}":$_POST['minute'];

unset($_POST['day']);
unset($_POST['month']);
unset($_POST['year']);
unset($_POST['hour']);
unset($_POST['minute']);

$agency_code = $_POST['agency_code'];
$_POST['tr_datereserve'] = $year.'-'.$month.'-'.$day.' '.$hour.':'.$minute.':00';
$_POST['tr_code'] = getCode($_POST['tr_datereserve'],'R',4);

$sql        = DBInsertPOST($_POST,'t_reserve');
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
if(intval($row['errorInfo'][0]) == 0){
  $id = $row['id'];
  $textQr   = $_POST['tr_code'].'|'.$agency_code;
  $CorrectionLevel = 'L';
  $matrixPointSize = 4;

  $str_rand = randomString(4,4);
  $filename = $str_rand.'_'.$_POST['tr_code'].'.png';
  QRcode::png($textQr, $new_path.$filename, $CorrectionLevel, $matrixPointSize, 2);

  $sql        = "UPDATE t_reserve SET tr_qrcode = '$filename' WHERE tr_id = '$id'";
  $query      = DbQuery($sql,null);

  $_POST['ref_code'] = $agency_code."#".$id;

  $ipAgency    = getIPbyAgency($agency_code);
  $data_array  = array(
                     "functionName" => "manageReserveQueue",  ///แก้ ชื่อ Service
                     "dataJson" => $_POST,
                   );

  $url        = "http://$ipAgency/queue_client/ws/service.php";

  // echo $url;
  // echo json_encode($data_array);

  $make_call = callAPI('POST', $url, json_encode($data_array));
  $response = json_decode($make_call, true);
  $status   = $response['status'];
  $data     = $response['data'];

  $arrUpdate['tr_id']       =  $id; //แก้ ID
  $arrUpdate['ref_code']    =  $_POST["ref_code"];

  // print_r($response);

  if($status == "200")
  {
      $arrUpdate['status_send']  =  "S";
  }
  $sql = DBUpdatePOST($arrUpdate,'t_reserve','tr_id'); ///แก้ ชื่อ table
  DbQuery($sql,null);

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'บันทึกสำเร็จ')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'ไม่สามารถบันทึกได้')));
}


?>
