$('#formRegister').on('submit', function(event) {
  event.preventDefault();
  if ($('#formRegister').smkValidate()) {
    // Code here
    $.ajax({
        url: 'service/AEDregister.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkAlert({text: data.message,type:data.status,position:'bottom-right'});
      if(data.status == 'success'){
        window.location = '../history/';
      }
    });
  }
});
