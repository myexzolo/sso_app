<div class="queue">
  <p>คิวทั้งหมดวันนี้</p>

  <?php
    $sql = "SELECT * FROM t_reserve r , t_agency a , t_service s
            WHERE r.agency_code = a.agency_code
            AND r.service_id = s.service_id
            AND r.mem_id = '{$_SESSION['member']['mem_id']}'
            AND r.tr_status = 'W'
            AND r.is_active = 'Y' ORDER BY r.tr_id Desc LIMIT 0,1 ";
    $query = DbQuery($sql,null);
    $json = json_decode($query,true);
    if($json['dataCount'] > 0){
      foreach ($json['data'] as $value) {
        $text = '';
        switch ($value['tr_status']) {
          case 'W':
            $text = 'รอการเช็คอินหน้าเครื่อง Kiosk';
            break;

          default:
            // code...
            break;
        }
  ?>
  <div class="box-queue">
    <div class="queue-header">
      คิวบริการ
    </div>
    <div class="queue-body">
      <div class="number"><?=$value['tr_code']?></div>
      <table class="table">
        <tr class="font-18">
          <td width="30%">เวลาจอง</td>
          <td><?=$value['tr_datereserve']?></td>
        </tr>
        <tr class="font-18">
          <td>งานบริการ</td>
          <td><?=$value['agency_name']?></td>
        </tr>
        <tr class="font-18">
          <td>หน่วยงาน</td>
          <td><?=$value['service_name']?></td>
        </tr>
        <tr class="font-18">
          <td class="text-center" colspan="2">
            <span class="text-qr" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Qr code</span>
            <div class="collapse" id="collapseExample">
              <img class="qrcode" src="../../image/qr/<?=$value['tr_qrcode']?>" alt="">
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <div class="status-queue">
    <div class="queue-repeat">
      <span class="glyphicon glyphicon-repeat"></span> อัพเดทคิว
    </div>
    <div class="header-status">สถานะคิว</div>
    <h1 class="text-center"><?=$text?></h1>
    <p>อัพเดทล่าสุดตอนนี้</p>
  </div>
  <?php } ?>
  <?php }else{ ?>

  <?php } ?>
</div>
