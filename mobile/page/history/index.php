<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>History : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>

    <?php $back='../home';$backname='กลับหน้าหลัก'; ?>
    <?php include "../../inc/back.php"; ?>
    <?php include "../../inc/nav.php"; ?>


    <?php
      $page = array('profile','queue');
      foreach ($page as $value) {
        include "pages/$value.php";
      }
    ?>

    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>
