<?php

function getMonth($month){

  $arr_month = array(
    '',
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม',
  );
  return $arr_month[$month];

}

function idCard($personID){
  $rev = strrev($personID);
  $total = 0;

  if(strlen($personID) == 13){
    for($i=1;$i<13;$i++){
     $mul = $i +1;
     $count = $rev[$i]*$mul;
     $total = $total + $count;
    }
    $mod = $total % 11;
    $sub = 11 - $mod;
    $check_digit = $sub % 10;
    return $rev[0] == $check_digit?true:false;
  }else{
    return false;
  }

}

function getCode($date,$title,$lenght){
  $sql = "SELECT COUNT(tr_id) AS num FROM t_reserve
          WHERE DATE_FORMAT(tr_datereserve, '%Y-%m-%d') = DATE_FORMAT('$date', '%Y-%m-%d')
          ORDER BY tr_id DESC LIMIT 0,1";
  $query = DbQuery($sql,null);
  $json = json_decode($query,true);
  if($json['data'][0]['num'] > 0){
    $code = $title.str_pad($json['data'][0]['num']+1,$lenght,"0",STR_PAD_LEFT);
  }else{
    $code = $title.str_pad(1,$lenght,"0",STR_PAD_LEFT);
  }
  return $code;
}


?>
