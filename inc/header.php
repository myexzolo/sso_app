<?php
if(!isset($_SESSION['member'])){
  exit("<script>window.location='../../pages/login/'</script>");
}
$USERIMG = isset($_SESSION['member'][0]['user_img'])?"../../image/user/".$_SESSION['member'][0]['user_img']:"../../image/user.png";
?>
<style>
.cut-text {
  text-overflow: ellipsis;
  overflow: hidden;
  width: 240px;
  white-space: nowrap;
  margin: auto;
}
</style>
<header class="main-header">


  <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini" data-toggle="push-menu" ><i class="icon ion-navicon-round" style="font-size: 18px;"></i></span>
    <!-- logo for regular state and mobile devices -->
    <i class="icon ion-navicon-round pull-left" data-toggle="push-menu" style="font-size: 18px;"></i>
    <span class="logo-lg">
       ระบบสนับสนุนการให้บริการ (Queue)
    </span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <!-- <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a> -->
    <!-- Navbar Right Menu -->
  <!--/nav> -->
</header>
