<?php
  include('connect.php');
  include('authen.php');
  include('mainFunc.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $baseurl = "/pages/";
  $REQUEST_URI = isset($_SESSION['RE_URI'])?$_SESSION['RE_URI']:"";

  $MEMBER = $_SESSION['member'];
  $module_idPage = "";
  $url = getUrlHost();


  $strArr = explode("/",$REQUEST_URI);
  $inx    = count($strArr) - 2;
  //$page_path = substr(str_replace($baseurl,'',$REQUEST_URI) , 0,-1);
  $page_path = $strArr[$inx];

  //echo $page_path;
  $active = "";


?>
<ul class="sidebar-menu tree" data-widget="tree">
        <li class="header" style="font-size:22px;background-color: #ecf0f5;">งานบริการ</li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li> -->
        <li class="<?=$page_path=="home"?"active":""?>">
          <a href="../../pages/home/">
            <i class="ion ion-android-home"></i> <span>หน้าหลัก</span>
            <!-- <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span> -->
          </a>
        </li>
        <li class="<?=$page_path=="reserve"?"active":""?>">
          <a href="../../pages/reserve/">
            <i class="ion ion-android-calendar"></i> <span>จองคิว</span>
          </a>
        </li>
        <li class="<?=$page_path=="queue_list"?"active":""?>">
          <a href="../../pages/queue_list/">
            <i class="ion ion-android-list"></i> <span>ประวัติการจองคิว</span>
          </a>
        </li>
        <li class="<?=$page_path=="queue_check"?"active":""?>">
          <a href="../../pages/queue_check/">
            <i class="ion ion-search"></i> <span>ตรวจสอบคิว</span>
          </a>
        </li>
        <li class="<?=$page_path=="queue_quest"?"active":""?>">
          <a href="../../pages/queue_quest/">
            <i class="ion ion-chatbox-working"></i> <span>แสดงความคิดเห็น</span>
          </a>
        </li>
        <li class="<?= $active ?>">
          <a onclick="logout()">
            <i class="fa fa-sign-out"></i> <span>ออกจากระบบ</span>
          </a>
        </li>
        <!-- <li>
          <a href="pages/mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li> -->
