<?php
  include('connect.php');
  include('authen.php');
  include('mainFunc.php');
  header("Content-type:text/html; charset=UTF-8");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);

  $baseurl = "/pages/";
  $REQUEST_URI = isset($_SESSION['RE_URI'])?$_SESSION['RE_URI']:"";

  $MEMBER = $_SESSION['member'][0];
  $module_idPage = "";
  $url = getUrlHost();

  // $sqlu = "SELECT GROUP_CONCAT(DISTINCT page_list ORDER BY page_list ASC SEPARATOR ',') as page_list,
  //                 GROUP_CONCAT(DISTINCT role_code ORDER BY role_code ASC SEPARATOR ',') as role_code
  //                 FROM t_role WHERE role_id IN ({$MEMBER['role_list']})";
  // //echo $sqlu;
  // $queryu = DbQuery($sqlu,null);
  // $rows   = json_decode($queryu, true);
  // $rowu   = $rows['data'];
  // $dataCount   = $rows['dataCount'];

  $dataCount = count(isset($_SESSION['ROLE_USER'])?$_SESSION['ROLE_USER']:0);

  $strPage = '';

  if($dataCount > 0)
  {
     $strPage   = $_SESSION['ROLE_USER']['page_list'];
     $roleCode  = $_SESSION['ROLE_USER']['role_code'];


    $strArr = explode("/",$REQUEST_URI);
    $inx    = count($strArr) - 2;
    //$page_path = substr(str_replace($baseurl,'',$REQUEST_URI) , 0,-1);
    $page_path = $strArr[$inx];

    $arrPage = array_unique(explode(",",$strPage));
    sort($arrPage);
    $arrPage = implode(",",$arrPage);
    $arrPage = substr($arrPage,1);
    //echo $arrPage;
    $rowp = $_SESSION['MENU'];
    //
    // $sqlp = "SELECT p.*,m.root_id,m.module_name,m.module_icon,r.root_name
    //          FROM t_page p , t_module m , t_root r
    //          WHERE p.page_id IN ($arrPage) AND p.is_active = 'Y'
    //          AND m.module_type = '1' AND m.is_active = 'Y' AND p.module_id = m.module_id
    //          AND m.root_id = r.root_id
    //          order by r.root_seq, m.module_order";
    // $queryp = DbQuery($sqlp,null);
    // $rows   = json_decode($queryp, true);
    // $rowp   = $rows['data'];
    $strRoot = '';
    $arrData = array();
    $arrRoot = array();
    $strs = '';

    $code       = $_SESSION['AGENCY_CODE'];
    $agency_code = $_SESSION['member'][0]['agency_code'];


    $sqla   = "SELECT * FROM t_agency
               where is_active not in ('D') and agency_code = '$agency_code'";
    //echo $sqla;

    $querya      = DbQuery($sqla,null);
    $jsona       = json_decode($querya, true);
    $dataCounta  = $jsona['dataCount'];
    $rowA        = $jsona['data'];
    $agency_name = $rowA[0]['agency_name'];

    ?>
    <li class="header" style="font-size: 21px;color:#224199; background-color: #f2f2f2;height: 40px;line-height: 24px;">
      <?=str_replace("สำนักงานประกันสังคม","",$agency_name)?>
    </li>
    <?php
    // echo print_r($rowp);

    foreach ($rowp as $k => $value)
    {
      $module_id  = $value['module_id'];
      $root_id    = $value['root_id'];
      $page_id    = $value['page_id'];
      $pagePath   = $value['page_path'];

      $strs     .= " ".$page_path;
      if($page_path == $pagePath){
        $module_idPage = $module_id;
      }
      $arrRoot[$root_id]['root_name'] = $value['root_name'];
      $arrData[$root_id][$module_id]["module_name"] = $value['module_name'];
      $arrData[$root_id][$module_id]["module_icon"] = $value['module_icon'];
      $arrData[$root_id][$module_id]["page"][$page_id] = $value;
    }

  // check Page Role
  $pagess = substr( str_replace($baseurl,"",$REQUEST_URI),0,-1 );
  $pos = strrpos($strs, $pagess);

  if ($pos === false) {
    //exit("<script>alert('ไม่มีสิทธิ์ใช้งาน');window.history.back();</script>");
  }
  // End Function  //

  $role_code  = explode(",",$_SESSION['ROLE_USER']['role_code']);
  foreach ($arrRoot as $key => $valueRoot)
  {
      $root_id = $key;
  ?>
<li class="header"><?=$valueRoot['root_name']?></li>
      <?php
      if(in_array("ADM", $role_code) && $key == 1){
        $sqla   = "SELECT *
                   FROM t_agency
                   where is_active not in ('D')
                   ORDER BY agency_code";

        $querya      = DbQuery($sqla,null);
        $jsona       = json_decode($querya, true);
        $dataCounta  = $jsona['dataCount'];
        $rowA        = $jsona['data'];

         //print_r($rowA);
      ?>
      <select id="agency_code" class="form-control select2" style="width: 100%;">
        <?php
          for ($s=0; $s < $dataCounta; $s++) {
            $agency_code  = $rowA[$s]['agency_code'];
            $agency_name  = $rowA[$s]['agency_name'];

            $selected =  ' ';
            if($code == $agency_code && $code != ""){
              $selected =  'selected="selected"';
            }
            echo '<option value="'.$agency_code.'" '.$selected.'>'.str_replace("สำนักงานประกันสังคม","",$agency_name).'</option>';
          }
        ?>
      </select>
      <?php
    }
    foreach ($arrData[$root_id] as $module_id => $valueModule)
    {
      $countPage    = count($valueModule["page"]);
        if($countPage == 1)
        {

        ?>
        <li class="<?=$module_id==$module_idPage?"active":""?>" >
          <?php
              foreach ($valueModule["page"] as $kay => $value) {
          ?>
          <a href="../<?=$value["page_path"]?>/" >
            <i class="fa <?=$valueModule["module_icon"]?>"></i>
            <span><?=$valueModule["module_name"]?></span>
          </a>
          <?php }?>
        </li>
        <?php
        }else{
        ?>
        <li class="treeview <?=$module_id==$module_idPage?"active menu-open":""?>">
        <a href="#">
          <i class="fa <?=$valueModule["module_icon"]?>"></i>
          <span><?=$valueModule["module_name"]?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php
              foreach ($valueModule["page"] as $kay => $value) {
          ?>
          <li class="<?=$page_path==$value['page_path']?"active":""?>"><a href="../<?=$value['page_path']?>/"><i class="<?=$value['page_icon'];?>"></i><?=$value['page_name']?></a></li>
          <?php } ?>
        </ul>
        </li>
        <?php
        }
    }
  }
}
  ?>
  <script>
    $(function () {
      $('.select2').select2();
      $("#agency_code").change(function(){changeAgency();});
    })

    function changeAgency()
    {
      var agencyCode = $("#agency_code").val();
      console.log(agencyCode);
      $.post("<?= $url?>inc/function/setAgency.php",{agencyCode:agencyCode})
        .done(function( data ) {
          gotoPage("<?= $url?>pages/home");
      });
    }
  </script>
