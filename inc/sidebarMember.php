<?php include('function/authen.php'); ?>
<style>
.cut-text2 {
  text-overflow: ellipsis;
  overflow: hidden;
  width: 150px;
  white-space: nowrap;
  margin: auto;
  line-height: 30px;
}
</style>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= $USERIMG ?>" onerror="this.onerror='';this.src='../../image/user.png'" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <div class="cut-text2" style="font-size: 20px;color:#444444;font-weight: 600;">
          <?= $_SESSION['member']['mem_name']." ".$_SESSION['member']['mem_last']?>
        </div>
        <a href="#"><i class="fa fa-circle text-success"></i><span style="font-size:16px;">Online<span></a>
      </div>
    </div>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree" id="showSlidebar">
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
